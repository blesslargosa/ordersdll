﻿using MongoDB.Driver;
using MongoDB.Driver.Builders;
using OrderingTool.Entities;
using OrderingTool.Interfaces.Classes;
using System;
using System.IO;
using System.Linq;

namespace OrderingTool {
	public class GenerateOrders {
		public void Call(string supplier, AllPlatforms allplatformsActions, string UserName = "unknown") {

			string LogFileName = AppDomain.CurrentDomain.BaseDirectory + "OrderFiles\\_GenerateOrdersErrors.txt";
			var db = DBCollections.GenerateOrderStatusCollection;
			var exportedCollection = DBCollections.ExportedCollection;

			try {

				// Update Status for View
				if (db.Find(Query.EQ("_id", supplier)).Count() == 0) {
					db.Insert(new GenerateOrderStatusEntity { sellerId = supplier, status = "<font color='green'>Exporting... Please wait.</font>", filename = "" });
				}

				var calculatedorders = allplatformsActions.GetCalculatedOrders();

				var orders = new GroupOrders();

				orders.groupedOrders = orders.getAllGroupedOrders(calculatedorders);

				var supplierGroupedOrders = orders.getGroupedItemsBySupplier(supplier);

				supplierGroupedOrders.OrderByDescending(x => x.itemsToOrderFomSupplier);

				var date = DateTime.Now;
				var dateTick = date.ToString("yyyy-MM-dd---HH-mm-ss-fff");
				string filename = "";

				ExportFile EF = new ExportFile();
				filename = EF.WriteCSV(supplierGroupedOrders, dateTick);
				orders.ProcessOrders(supplierGroupedOrders, dateTick);

                if (filename == "")
                {
                    db.Update(Query.EQ("_id", supplier), Update.Set("filename", "I'm sorry. I just don't know what went wrong. :-("));
                    db.Update(Query.EQ("_id", supplier), Update.Set("status", "<small><a href='VeOrder/GetStatusError?supplier=" + supplier + "' target='_blank' onclick=\"$('#orderDiv_" + supplier + "').html('<small>Error (please refresh page)</small>');\" style='color:red'>Error (unknown error)</a></small>"));
                }
                else
                {

                      db.Remove(Query.EQ("_id", supplier));
                    //db.Update(Query.EQ("_id", supplier), Update.Set("filename", filename));

                    //db.Update(Query.EQ("_id", supplier), Update.Set("status", "<a href='javascript:;' onclick=\"$('#orderDiv_" + supplier + "').html('<small>Downloaded</small>');"));
                    
                    //db.Update(Query.EQ("_id", supplier), Update.Set("status", "<a href='javascript:;' onclick=\"$('#orderDiv_" + supplier + "').html('<small>Downloaded</small>'); window.location='VeOrder/DownloadGeneratedOrder?supplier=" +
                    //    supplier + "';\">Download File</a>"));
                }

				exportedCollection.Update(Query.EQ("_id", supplier), Update.Set("exportedDate", date), UpdateFlags.Upsert);

				new OrderToolDB_GroupedItems().UpdateExportedBy(Path.GetFileNameWithoutExtension(filename), UserName);
			}
			catch (Exception ex) {
				File.AppendAllText(LogFileName, DateTime.Now.ToString("[yyyy-MM-dd---HH-mm-ss-fff] ") + ex.Message + Environment.NewLine);

				db.Update(Query.EQ("_id", supplier), Update.Set("filename", ex.Message));
				db.Update(Query.EQ("_id", supplier), Update.Set("status", "<small><a href='VeOrder/GetStatusError?supplier=" + supplier + "' target='_blank' onclick=\"$('#orderDiv_" + supplier + "').html('<small>Error (please refresh page)</small>');\" style='color:red'>Error (click to view)</a></small>"));
			}

		}
	}
}

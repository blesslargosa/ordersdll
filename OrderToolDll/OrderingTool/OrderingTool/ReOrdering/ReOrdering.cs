﻿using DN_Classes.Entities;
using DN_Classes.Products.Price;
using MongoDB.Bson;
using OrderingTool.Interfaces;
using OrderingTool.Interfaces.Classes;
using OrderingTool.QueriesCommands.ReOrders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WhereToBuy;

namespace OrderingTool
{
    public class ReOrdering
    {
      
        ReOrderItemsCommands reOrder = new ReOrderItemsCommands();

        AllPlatforms allplatformsActions = null;


        public ReOrdering() {

            Amazon amazon = new Amazon();
            Ebay ebay = new Ebay();
            ReOrdered reordered = new ReOrdered();

            allplatformsActions = new AllPlatforms(amazon, ebay, reordered);
        
        }

        public void ReOrderItemEanOrderId(string ean, DateTime reOrderDate, string note)
        {
            CheapestPrice getCheapestSupplier = new CheapestPrice();
            BuyPriceRow row = new BuyPriceRow(ean);
         

            try { row = getCheapestSupplier.GetCheapestPriceByEan(ean, ""); }
            catch { }

            ObjectId id = reOrder.ImportReOrderItemByEan(ean,row, reOrderDate);
            reOrder.UpdateReOrderItemByEan(id, row, note, reOrderDate);
        }

        public string GetWhereToBuySupplier(string ean) {

            CheapestPrice getCheapestSupplier = new CheapestPrice();
            BuyPriceRow row = new BuyPriceRow(ean);
            try { row = getCheapestSupplier.GetCheapestPriceByEan(ean, ""); }
            catch { }

            return row.suppliername.ToString();
         
        }

        public void ReOrderItemEanOrderId(string ean, string orderId, DateTime reOrderDate, string note)
        {
            ISingleAction platformAction = new ReOrdered();
            var item = ReOrderItem(ean, orderId, reOrderDate, note);

            string reOrderId = DateTime.Now.Ticks.ToString();

            reOrder.ImportReOrderItem(item, reOrderId, GetWhereToBuySupplier(ean));
            string supplierOrderId = "";
            if (item.plus != null && item.plus.Contains("supplierOrderId")) {

                supplierOrderId = item.plus["supplierOrderId"].ToString();
            }
                platformAction.UpdateReOrderedItem(item.Ean, item.AmazonOrder.AmazonOrderId, reOrderDate, note, supplierOrderId, reOrderId);
            

        }

        public string ReOrderItemEanOrderId(string ean, string orderId, DateTime reOrderDate, string note , string a)
        {
            ISingleAction platformAction = new ReOrdered();
            var item = ReOrderItem(ean, orderId, reOrderDate, note);

            string reOrderId = DateTime.Now.Ticks.ToString();
            string cheapestSupplier = GetWhereToBuySupplier(ean);
            reOrder.ImportReOrderItem(item, reOrderId, cheapestSupplier);
            string supplierOrderId = "";
            if (item.plus != null && item.plus.Contains("supplierOrderId"))
            {

                supplierOrderId = item.plus["supplierOrderId"].ToString();
            }
            platformAction.UpdateReOrderedItem(item.Ean, item.AmazonOrder.AmazonOrderId, reOrderDate, note, supplierOrderId, reOrderId);

            return cheapestSupplier;
        }

        public CalculatedProfitEntity ReOrderItem(string ean, string orderId, DateTime reOrderDate, string note)
        {
            ISingleAction platformAction = null;

            platformAction = allplatformsActions.GetOrderPlatform(ean, orderId);
            var order = platformAction.GetSpecificOrderByOrderId(ean, orderId);
            string supplierOrderId = platformAction.GetSupplierOrderId(order);
            platformAction.UpdateReOrderedItem(ean, orderId, reOrderDate, note,supplierOrderId,"");

            return order;
        }

    }
}

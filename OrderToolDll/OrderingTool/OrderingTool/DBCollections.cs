﻿using DN_Classes.Entities;
using DN_Classes.Supplier;
using MongoDB.Bson;
using MongoDB.Driver;
using OrderingTool.Entities;

namespace OrderingTool {
	public class DBCollections {



		public static string amazon = Properties.Settings.Default.AmazonSingleCalculated.ToString();
		public static string ebay = Properties.Settings.Default.EbaySingleCalculated.ToString();
		public static string grouporders = Properties.Settings.Default.GroupedOrders.ToString();
		public static string reorder = Properties.Settings.Default.ReOrder.ToString();
		public static string server = Properties.Settings.Default.Server.ToString();
        public static string generatestatus = Properties.Settings.Default.GenerateOrderStatus.ToString();
        public static string exportstatus = Properties.Settings.Default.ExportStatus.ToString();

		private static MongoCollection<T> GetCollection<T>(string dbName, string collectionName) {
			return new MongoClient(@"mongodb://steffennordquist:adminsteffen148devnetworks@" + server + ":27017/" + dbName + "?authSource=admin").GetServer()
				.GetDatabase(dbName).GetCollection<T>(collectionName);
		}

		public static MongoCollection<CalculatedProfitEntity> Collection = GetCollection<CalculatedProfitEntity>("CalculatedProfit", amazon);

		public static MongoCollection<GenerateOrderStatusEntity> GenerateOrderStatusCollection = GetCollection<GenerateOrderStatusEntity>("CalculatedProfit", generatestatus);

        public static MongoCollection<ExportedSupplierEntity> ExportedCollection = GetCollection<ExportedSupplierEntity>("CalculatedProfit", exportstatus);


		public static MongoCollection<CalculatedProfitEntity> EbayCalculatedCollection = GetCollection<CalculatedProfitEntity>("CalculatedProfit", ebay);

		public static MongoCollection<GroupedItemsEntity> GroupedItems = GetCollection<GroupedItemsEntity>("CalculatedProfit", grouporders);

		public static MongoCollection<SupplierEntity> SupplierCollection = GetCollection<SupplierEntity>("Supplier", "supplier");

		public static MongoCollection<CalculatedProfitEntity> ReOrderCollection = GetCollection<CalculatedProfitEntity>("CalculatedProfit", reorder);

		public static MongoCollection<JtlEntity> JtlCollection = GetCollection<JtlEntity>("PD_JTL", "jtl");

		//------------RETURNS BSON-----------------------------------------

		public static MongoCollection<BsonDocument> EbayCollectionBson = GetCollection<BsonDocument>("CalculatedProfit", ebay);

		public static MongoCollection<BsonDocument> ReOrderCollectionBson = GetCollection<BsonDocument>("CalculatedProfit", reorder);

		public static MongoCollection<BsonDocument> AmazonCollectionBson = GetCollection<BsonDocument>("CalculatedProfit", amazon);

	}
}
﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderingTool.OrderPurchaseFormatting.ExportingFormats
{
    public class TxtEanStock : IExportFormat
    {
        private List<GroupedItemsEntity> Groupedorders;
        private string Filename;

        public void Prepare(List<GroupedItemsEntity> groupedorders, string filename)
        {
            this.Groupedorders = groupedorders.Where(x => x.packingUnitsToOrderFromSupplier > 0).OrderByDescending(x => x.packingUnitsToOrderFromSupplier).ToList();
            this.Filename = filename;
        }

        public void Write()
        {
            List<string> eansStock = Groupedorders.Where(x => !String.IsNullOrEmpty(x.ean)).Select(x => x.ean + ";" + x.itemsToOrderFomSupplier).ToList();
            File.WriteAllText(Filename, String.Join("\r\n", eansStock));
        }
    }
}

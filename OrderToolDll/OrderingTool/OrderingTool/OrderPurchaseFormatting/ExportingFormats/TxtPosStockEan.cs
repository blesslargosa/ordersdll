﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderingTool.OrderPurchaseFormatting.ExportingFormats
{
    public class TxtPosStockEan : IExportFormat
    {
        private List<GroupedItemsEntity> Groupedorders;
        private string Filename;

        public void Prepare(List<GroupedItemsEntity> groupedorders, string filename)
        {
            this.Groupedorders = groupedorders.Where(x => x.packingUnitsToOrderFromSupplier > 0).OrderByDescending(x => x.packingUnitsToOrderFromSupplier).ToList();
            this.Filename = filename;
        }

        public void Write()
        {
            Groupedorders = Groupedorders.Where(x => !String.IsNullOrEmpty(x.ean)).ToList();
            List<string> posStockEan = new List<string>();
            int count = 1;
            foreach (var x in Groupedorders) {
                posStockEan.Add("Pos." + count + "\t" + x.itemsToOrderFomSupplier + " Stück" + "\t" + x.singleOrders.First().ArticleNumber);
            count++;
            }
            File.WriteAllText(Filename, String.Join("\r\n", posStockEan));
        }
    }
}

﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderingTool.OrderPurchaseFormatting.FormatIndicators
{
    public class PlusSectionIndicator : IFormatIndicator
    {
        private BsonDocument plus = null;

        public PlusSectionIndicator(BsonDocument plus)
        {
            this.plus = plus;

        }

        public string GetIndicator()
        {
            string indicator = "";

            if (plus != null && plus.Contains("ExportFormatIndicator"))
            {
                BsonValue indicatorValue = plus["ExportFormatIndicator"].AsBsonValue;
                if (indicatorValue != null && indicatorValue.IsString)
                {
                    indicator = indicatorValue.AsString;
                }
            }

            return indicator;
        }
    }
}

﻿using DN_Classes.Entities;
using MongoDB.Bson;
using OrderingTool.Entities;
using OrderingTool.Interfaces;
using OrderingTool.Interfaces.Classes;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace OrderingTool {
	public class GroupOrders
    {

       public List<GroupedItemsEntity> groupedOrders { get; set; }

      AllPlatforms allplatformsActions = null;


       public GroupOrders()
        {
            Amazon amazon = new Amazon();
            Ebay ebay = new Ebay();
            ReOrdered reordered = new ReOrdered();
            allplatformsActions = new AllPlatforms(amazon, ebay, reordered);
        }
      
       public List<GroupedItemsEntity> getAllGroupedOrders(List<CalculatedProfitEntity> calculatedorders) {

           List<GroupedItemsEntity> groupedOrders = calculatedorders.GroupBy(x => new { x.Ean, x.SupplierName, x.ArticleNumber }).Select(y => new GroupedItemsEntity
             (
                 y.Key.Ean,
                 y.Key.SupplierName,
                 y.Key.ArticleNumber,
                 y.Count(),
                 y.First().VE,
                 y.First().Item.NewBuyPrice,
                 y.Sum(z => z.Item.NewBuyPrice),
                 y.Min(a => a.PurchasedDate),
                 y.ToList()          
                  )).ToList();
            //y.Key.Ean,
            //     y.Key.SupplierName,
            //     y.Key.ArticleNumber,
            //     y.ToList().Count(),
            //     y.GroupBy(w => w.VE).Last().Key,
            //     y.GroupBy(z => z.Item.NewBuyPrice).First().Key,
            //     y.Sum(z => z.Item.NewBuyPrice),
            //     y.Min(a => a.PurchasedDate),
            //     y.ToList()
                
            //      )).ToList();
           //,
                this.groupedOrders = groupedOrders;

           return groupedOrders;
       }

       public void ProcessOrders(List<GroupedItemsEntity> groupedOrders, string dateTick)
       {
           List<ObjectId> ids = new List<ObjectId>();
           List<ISingleAction> platforms = new List<ISingleAction>();
           OrderToolDB_GroupedItems ordersdb = new OrderToolDB_GroupedItems();
           ISingleAction action = null;
          
           foreach (var groupedorder in groupedOrders)
           {
              
               foreach( var orderId_Id in allplatformsActions.GetAllOrderIds(groupedorder))
               {
                   try
                   {
                       //update ebay and amazon single calculated 
                       action = allplatformsActions.GetOrderPlatform(groupedorder.ean, orderId_Id);
                       if (action != null){ platforms.Add(action); }
                       platforms.Add(new ReOrdered());

                       foreach (ISingleAction platform in platforms)
                       {
                           try{
                               if (platform != null)
                               {
                                   List<ObjectId> _ids = platform.Get_Id(groupedorder.ean, orderId_Id, groupedorder.supplier);
                                   if (_ids.Count == 0)
                                   {
                                       _ids.Add(ObjectId.Parse(orderId_Id));
                                   }
                                   _ids.ForEach(x => platform.UpdateCalculatedOrder(x, groupedorder.supplier, dateTick));
                               }
                           }catch{}
                       }
                       
                       
                   }
                   catch { 
                   
                   }
               }
               ordersdb.SaveOrderedItems(groupedorder, dateTick);
           }
       }

       public  List<GroupedItemsEntity> getGroupedItemsBySupplier(string supplierName)
       {
           return groupedOrders.Where(x => x.supplier == supplierName).ToList();
       }

       public List<string> getSupplierNames()
       {
           return groupedOrders.GroupBy(x => x.supplier).Select(x => x.Key).ToList();
       }

       public double getOrderAmountBySupplier(string supplierName)
       {
           return getGroupedItemsBySupplier(supplierName).Sum(x => x.totalPackingUnitsPrice);
       }

       public List<GroupedSupplierOrderIdItems> GetGroupedItemsBySupplierOrderId(List<GroupedItemsEntity> groupedItems)
       {

        var groupedOrdersBySupplierOrderId = groupedItems.GroupBy( x=>x.supplierOrderId).Select(y => new GroupedSupplierOrderIdItems(
           
           //y.GroupBy(v => v.supplierOrderId).First().Key,
           //y.ToList(),
           //ConvertToDate(y.GroupBy(v => v.supplierOrderId).First().Key),


            y.GroupBy(v => v.supplierOrderId).First().Key,
            y.ToList(),
            ConvertToDate(y.GroupBy(v => v.supplierOrderId).First().Key),
            CheckOrdered(y.ToList().First()),
            CheckExportedBy(y.ToList().First())
            //y.Where(v => v.plus["exportedBy"] != null).DefaultIfEmpty(string.Empty)
            

           // y.GroupBy(v => v.supplierOrderId).First().Where( s => CheckOrdered(s) == true).First(),
           // y.Where( GroupBy(v => v.supplierOrderId).First().Where(s => CheckExportedBy(s) == true).Select(x => x.plus["exportedBy"]).First().ToString()
           
           )).ToList();

        return groupedOrdersBySupplierOrderId;
       }

		private DateTime ConvertToDate(string p) {
			try {
				return DateTime.ParseExact(p.Split('_')[1], "yyyy-MM-dd---HH-mm-ss-fff", CultureInfo.InvariantCulture);
			}
			catch {
				return new DateTime(long.Parse(p.Split('_')[1]));
			}
		}

        private bool CheckOrdered(GroupedItemsEntity groupedOrders)
        {

            if (groupedOrders.plus != null && groupedOrders.plus.Contains("orderedFromSupplier"))
            { return groupedOrders.plus["orderedFromSupplier"].AsBoolean; }
            else { return false; }
        }

        private string CheckExportedBy(GroupedItemsEntity groupedOrders)
        {

            if (groupedOrders.plus != null && groupedOrders.plus.Contains("exportedBy"))
            { return groupedOrders.plus["exportedBy"].AsString; }
            else { return ""; }
        } 
    }
}

﻿using DN_Classes.Entities;
using DN_Classes.Supplier;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using OrderingTool.Entities;
using OrderingTool.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace OrderingTool
{
    public class OrderToolDB_GroupedItems : DBCollections
    {

     
        /// <summary>
        /// this will get all exported/ ordered items
        /// </summary>
        /// <returns>List<GroupedItemsEntity></returns>
        public List<GroupedItemsEntity> GetExportedOrders()
        {
            try
            {
                   return GroupedItems.FindAll().ToList();
              

            }
            catch (Exception msg) { }

            return null;
        }

        public List<GroupedItemsEntity> GetSupplierOrderIds()
        {

            return GroupedItems.FindAll().SetFields(Fields.Include("supplierOrderId")).ToList();
        }

        private bool GetCurrent(string p)
        {
            // //KNV_2015-12-17---08-51-31-955
            string pattern = "[A-Za-z]_(\\d{4})-(\\d{2})-(\\d{2})---(\\d{2})-(\\d{2})-(\\d{2})-(\\d{3})";

            Regex regex = new Regex(@pattern);
            Match match = regex.Match(p);
            if (match.Success)
            {
                return true;
            }
            else { return false; }
           
        }

        /// <summary>
        /// save single generated item with datetime as ID
        /// </summary>
        /// <param name="groupedItemsEntity"></param>
        /// <param name="dateTick"></param>
        public void SaveOrderedItems(GroupedItemsEntity groupedItemsEntity, String dateTick)
        {
            groupedItemsEntity.supplierOrderId = groupedItemsEntity.supplier + "_" + dateTick;
            GroupedItems.Insert(groupedItemsEntity);
        }

        /// <summary>
        /// get grouped orders by supplier
        /// </summary>
        /// <param name="supplier"></param>
        /// <returns>List<GroupedItemsEntity></returns>
        public List<GroupedItemsEntity> GetGeneratedGroupedItems(string supplier)
        {

            return GroupedItems.Find(Query.EQ("supplier", supplier)).ToList();

        }

        /// <summary>
        /// get grouped orders by supplierOrderId
        /// </summary>
        /// <param name="supplierOrderID"></param>
        /// <returns>List<GroupedItemsEntity></returns>
        public List<GroupedItemsEntity> GetOrdersBySupplierOrderId(string supplierOrderId)
        {

            return GroupedItems.Find(Query.EQ("supplierOrderId", supplierOrderId)).ToList();

        }



        public void UpdateOrderedFromSupplier(string supplierOrderId, bool value) {

            GroupedItems.Update(Query.EQ("supplierOrderId", supplierOrderId), Update.Set("plus.orderedFromSupplier", value), UpdateFlags.Multi);

        }

        public void UpdateExportedBy(string supplierOrderId, string name)
        {

            GroupedItems.Update(Query.EQ("supplierOrderId", supplierOrderId), Update.Set("plus.exportedBy", name), UpdateFlags.Multi);

        }

		public void UpdateExpected(string supplierOrderId, int newUnitSize,string ean)
		{
			GroupedItems.Update(Query.And(Query.EQ("supplierOrderId", supplierOrderId),Query.EQ("ean",ean)), Update.Set("plus.OrderedExpectedArrived.expected", newUnitSize), UpdateFlags.Multi);
		}


		public int GetPackingUnitSize(string ean)
		{
			int unitsize = 0;

			try
			{
				unitsize = GroupedItems.FindOne(Query.EQ("ean", ean)).packingUnitsToOrderFromSupplier;
			}
			catch { }
			


			return unitsize;
		}


		public void UpdateIsEdittedSpecificItem(string supplierOrderId, bool isEditted, string ean)
		{

			GroupedItems.Update(Query.And(Query.EQ("supplierOrderId", supplierOrderId), Query.EQ("ean", ean)), Update.Set("plus.OrderedExpectedArrived.expectedIsEditted", isEditted), UpdateFlags.Multi);
		}

		public void UpdateIsEditted(string supplierOrderId, bool isEditted)
		{
			GroupedItems.Update(Query.EQ("supplierOrderId", supplierOrderId), Update.Set("plus.OrderedExpectedArrived.expectedIsEditted", isEditted), UpdateFlags.Multi);

		}
       
    }
}
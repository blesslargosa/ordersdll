﻿using DN_Classes.Supplier;
using MongoDB.Driver.Builders;
using ProcuctDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderingTool
{
   public  class OrderToolDB_AddOns: DBCollections
    {

        /// <summary>
        /// will get all suppliers
        /// </summary>
        /// <returns></returns>
        public List<SupplierEntity> GetSuppliers()
        {

            return SupplierCollection.FindAll().ToList();

        }

        /// <summary>
        /// will get stocks of a specific item from jtl/warehouse
        /// </summary>
        /// <param name="ean"></param>
        /// <returns></returns>
        public List<JtlEntity> GetStocksFromWarehouse(string ean)
        {
            return JtlCollection.Find(Query.EQ("product.ean", ean)).ToList();

        }

        


    }
}

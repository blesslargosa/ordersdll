﻿using DN_Classes.Entities;
using DN_Classes.Entities.ProcessProfitOrder;
using MongoDB.Bson;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace OrderingTool.Interfaces.Classes
{
    public class Ebay : DBCollections, ISingleAction, IEbayAmazonActions
    {
        public List<CalculatedProfitEntity> GetCalculatedOrders()
        {
            var ebayOrders = EbayCalculatedCollection.Find(Query.Or
                (
                Query.And(
                Query.NotExists("plus.supplierOrderId"),
                Query.NotExists("plus.excludeFromOrdering"))
                ,
                Query.And(
                Query.NotExists("plus.supplierOrderId"),
                Query.Exists("plus.excludeFromOrdering"),
                Query.EQ("plus.excludeFromOrdering", false))
                )).ToList();

            return ebayOrders;
        }

        public List<CalculatedProfitEntity> GetCalculatedOrdersPerSupplier(string suppliername)
        {
            return EbayCalculatedCollection.Find(Query.EQ("SupplierName", suppliername)).ToList();
        }

        public BsonDocument GetSpecificOrder(string ean, string supplier)
        {
            return EbayCollectionBson.FindOne(Query.And(Query.EQ("SupplierName", supplier), Query.EQ("Ean", ean)));
        }

        public void SetCalculatedOrderToOrdered(string OrderId, string supplierOrderId, string ean, string objectid)
        {
            if (OrderId != null)
            {
                EbayCalculatedCollection.Update(Query.And(Query.EQ("EbayOrder.OrderID", OrderId), Query.EQ("Ean", ean)), Update.Set("plus.supplierOrderId", supplierOrderId).Set("plus.ordered", true));
            }
            else
            {
                EbayCalculatedCollection.Update(Query.And(Query.EQ("_id", ObjectId.Parse(objectid)), Query.EQ("Ean", ean)), Update.Set("plus.supplierOrderId", supplierOrderId).Set("plus.ordered", true));
            }
        }

        public void UpdateCalculatedOrder(ObjectId objectId, string supplier, string dateTick)
        {
           
                string obj = objectId.ToString().Replace("{", "").Replace("}", "");

                var order = EbayCalculatedCollection.FindOne(Query.EQ("_id", ObjectId.Parse(obj)));

                if (order.plus == null || !order.plus.Contains("supplierOrderId"))
                {
                    EbayCalculatedCollection.Update(Query.EQ("_id", ObjectId.Parse(obj)),
                                 Update.Set("plus.supplierOrderId", supplier + "_" + dateTick)
                                 .Set("plus.ordered", true).Set("plus.excludeFromOrdering", true));
                }
        }


        public CalculatedProfitEntity GetSpecificOrderByOrderId(string ean, string orderId)
        {
            return EbayCalculatedCollection.FindOne(Query.And(Query.EQ("Ean", ean), Query.EQ("EbayOrder.OrderID", orderId)));
        }

        public string GetEanByOrderId(string orderId)
        {
            var orderedItems = EbayCalculatedCollection.Find(Query.EQ("EbayOrder.OrderID", orderId));

            if (orderedItems.Count() == 1)
            {

                return orderedItems.First().Ean;
            }

            return ""; 
        }


        public ISingleAction GetOrderPlatform(string ean, string orderId)
        {
            if (GetSpecificOrderByOrderId(ean, orderId) != null)
            {

                return new Ebay();
            }
            return null;
        }


        public List<CalculatedProfitEntity> GetSameOrders(string ean, string suppliername)
        {
            return EbayCalculatedCollection.Find(Query.And(Query.EQ("Ean", ean), Query.EQ("SupplierName", suppliername))).ToList();
        }


        public List<string> GetAllOrderIds(GroupedItemsEntity order)
        {
            return order.singleOrders.Where(x => x.EbayOrder != null).Select(x => x.EbayOrder.OrderID).ToList();
        }


        public List<ObjectId> Get_Id(string ean, string orderId, string suppliername)
        {
            List<ObjectId> ids = new List<ObjectId>();
            var orders = EbayCalculatedCollection.Find(Query.And(Query.EQ("Ean", ean), Query.EQ("EbayOrder.OrderID", orderId), Query.EQ("SupplierName", suppliername))).ToList();

            orders.ForEach(x => ids.Add(x._id));
            return ids;
        }


        public string GetSupplierOrderId(CalculatedProfitEntity order)
        {
            if (order.plus["supplierOrderId"] != null)
            {
                return order.plus["supplierOrderId"].ToString();
            }

            return "";
        }

        public void UpdateReOrderedItem(string ean, string orderId, DateTime ReOrderDate, string note, string oldsupplierOrderId, string reOrderId)
        {
            EbayCalculatedCollection.Update(Query.And(Query.EQ("Ean", ean), Query.EQ("EbayOrder.OrderID", orderId)),
            Update.Set("plus.note", note).
            Push("plus.reOrderDate", ReOrderDate));
        }


        public List<CalculatedProfitEntity> GetCalculatedOrdersByEan(string ean)
        {
            return EbayCalculatedCollection.Find(Query.EQ("Ean", ean)).ToList();
        }


        public BsonDocument GetSpecificBsonOrder(ObjectId id)
        {
            return EbayCollectionBson.FindOne(Query.EQ("_id", id));
        }


        public List<string> GetMultipleEansByOrderId(string orderId)
        {
            var orders = EbayCalculatedCollection.Find(Query.EQ("EbayOrder.OrderID", orderId)).ToList();

            return orders.Select(x => x.Ean).ToList();
        }


        public List<EanCountEntity> GetAllEansSold(DateTime date,string supplier)
        {
			var query = (String.IsNullOrEmpty(supplier) || supplier.ToLower().Trim() == "All Supplier".ToLower()) ? 
				Query.GTE("PurchasedDate", date) :
				Query.And(Query.GTE("PurchasedDate", date),
							Query.EQ("SupplierName", BsonRegularExpression.Create( 
																					new Regex(supplier,RegexOptions.IgnoreCase)	 
																					) ));

			var eans = EbayCalculatedCollection.Find(query).SetFields("Ean", "EbayOrder.TransactionArray", "SupplierName")
															.Select(x => new EanCountEntity { ean = x.Ean,
																								title = x.EbayOrder.TransactionArray.First().Item.Title, 
																								supplier = x.SupplierName }
															).ToList();
           return eans;
        }

		public List<EanCountEntity> GetAllEansSold(DateTime date)
		{
			var eans = EbayCalculatedCollection.Find(Query.GTE("PurchasedDate", date)).Select(x => new EanCountEntity { ean = x.Ean, title = x.EbayOrder.TransactionArray.First().Item.Title, supplier = x.SupplierName }).ToList();

			return eans;
		}


        public List<CalculatedProfitEntity> GetOrdersByOrderId(string orderId)
        {
            return EbayCalculatedCollection.Find(Query.EQ("EbayOrder.OrderID", orderId)).ToList();

        }
    }
}

﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderingTool.Entities
{
	[BsonIgnoreExtraElements]
	public class PlatformOrderDetails
    {
        public string PlatFormOrderId { get; set; }
        public DateTime PurchaseDate { get; set; }
        public string BuyerEmail { get; set; }
        public string BuyerName { get; set; }
        public string LatestShipDate { get; set; }
    }
}

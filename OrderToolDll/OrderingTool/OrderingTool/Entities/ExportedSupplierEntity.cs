﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderingTool.Entities
{
    public class ExportedSupplierEntity
    {
        [BsonId]
        public string supplier { get; set; }
        public DateTime exportedDate { get; set; }
    }
}

﻿
using MongoDB.Bson.Serialization.Attributes;
using OrderingTool;
using System;
using System.Collections.Generic;

namespace OrderingTool.Entities
{
	[BsonIgnoreExtraElements]
	public class GroupedSupplierOrderIdItems
    {
        public string supplierOrderId { get; set; }

        public List<GroupedItemsEntity> groupedOrders { get; set; }

        public DateTime dateExported { get; set; }

        public bool orderedFromSupplier { get; set; }
        public string exportedBy { get; set; }

        public GroupedSupplierOrderIdItems(string supplierOrderId, List<GroupedItemsEntity> groupedOrders, DateTime dateExported, bool ordered = false,string exportedBy = "")
        {
            this.supplierOrderId = supplierOrderId;
            this.groupedOrders = groupedOrders;
            this.dateExported = dateExported;
            this.orderedFromSupplier = ordered;
            this.exportedBy = exportedBy;
           
        }
    }
}

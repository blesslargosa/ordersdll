﻿
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace OrderingTool
{
	[BsonIgnoreExtraElements]
	public class JtlEntity
    {
        public ObjectId id { get; set; }

        public Product product { get; set; }

        public BsonDocument plus { get; set; }
    }
}

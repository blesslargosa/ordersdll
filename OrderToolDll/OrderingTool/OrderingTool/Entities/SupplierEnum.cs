﻿// Decompiled with JetBrains decompiler
// Type: OrderingTool.Entities.SupplierEnum
// Assembly: OrderingTool, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 44FB2E5F-4B66-4E92-8AC0-6B1621E3360C
// Assembly location: D:\REPO\mvc-webtool\AmazonOrders\AmazonOrders\bin\OrderingTool.dll

namespace OrderingTool.Entities
{
    public enum SupplierEnum
    {
        bvw,
        libri,
        knv,
        hoffmann,
        gallay,
        berk,
        sww,
        jpc,
        remember,
        schmidt,
        soundofspirit,
        ravensburger,
        moses,
        nlg,
        kosmos,
        goki,
        baer,
        boltze,
        clementoni,
        vtech,
        haba,
        herweck,
        jtl,
        norisspiele,
        primavera,
        bieco,
        fischer,
        moleskine,
        gruffelo,
        loqi,
        gardena,
        fabercastell,
        studio100,
        leuchtturm,
        wuerth,
        sheepworld,
        jobo
    }
}

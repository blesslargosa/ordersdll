﻿
using DN_Classes.Entities;
using MongoDB.Bson.Serialization.Attributes;
using System.Collections.Generic;

namespace OrderingTool.Entities
{
	[BsonIgnoreExtraElements]
	public class ItemsFromOtherSupplierEntity
    {
        public string supplierName { get; set; }

        public List<CalculatedProfitEntity> items { get; set; }
    }
}

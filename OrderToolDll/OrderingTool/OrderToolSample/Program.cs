﻿using DN_Classes.Entities;
using OrderingTool;
using OrderingTool.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using System.IO;

namespace OrderToolSample
{
    class Program
    {

       

        static void Main(string[] args)
        {
             

             OrderToolDB ordersdb = new OrderToolDB();
             List<CalculatedProfitEntity> calculatedorders = ordersdb.GetCalculatedOrders();

             Orders orders = new Orders(calculatedorders);
           
              //orders.ProcessOrders();

             foreach (var supplierName in orders.getSupplierNames())
             {
                 Console.WriteLine(supplierName);
                 Console.WriteLine(supplierName + " " + orders.getOrderAmountBySupplier(supplierName));
                 var suppliersGroupedItems = orders.getGroupedItemsBySupplier(supplierName);
                 suppliersGroupedItems.ForEach(x => Console.WriteLine(x.ean + " " + x.numberOfOrderedItems + " " + x.packingUnitsToOrderFromSupplier));
                 Console.Read();
             }
            Console.Read();

           

        }

       

        

      

     
        
        
    }
}

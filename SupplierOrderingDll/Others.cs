﻿using DN_Classes.Supplier;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using ProcuctDB.Suppliers;
using SupplierOrderingDLL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SupplierOrderingDLL
{
    public class Others :DBCollections
    {
        public int GetMultiplier(string sku)
        {
            if (sku != null)
            {
                string skuPattern = "[A-Z0-9]{1,15}[-][A-Z0-9]{10}[-][0-9]{13}[-][0-9]{1,5}";

                Match matchSku = Regex.Match(sku, skuPattern);

                if (matchSku.Success == true)
                {
                    try { return Convert.ToInt32(sku.Split('-')[3]); }
                    catch { return 1; }
                }
                else
                {
                    return 1;
                }
            }
            else {
                return 1;
            }
        }

        public int GetStockFromWareHouse(string ean)
        {
            JTLEntity item = JtlCollection.FindOne(Query.EQ("product.ean", ean));
            if (item == null)
            {
                return 0;  
            }
            return item.plus["quantity"].AsInt32;
        }

        public List<SupplierEntity> GetSuppliers()
        {

            return SupplierCollection.FindAll().ToList();

        }

        public bool CheckExportStatus(string supplierName)
        {
            List<string> suppliersExportedin16hr = GetAllSUppliersExported();

            if (suppliersExportedin16hr.Contains(supplierName)) return true;
            else return false;
        }

        private List<string> GetAllSUppliersExported()
        {
            var supp = ExportedCollection.FindAll().Where(x => x.exportedDate >= DateTime.Now.AddHours(-16));

            return supp.Select(x => x.supplier.ToString()).ToList();

        }

        public DateTime GetLastExportedDateTime(string supplierName) {

          
            var supplier = ExportedCollection.FindOne(Query.EQ("_id", supplierName));

            if (supplier == null) {

                return DateTime.MinValue;
            }
            return supplier.exportedDate;
        
        
        }

        public int GetCountDaysPassedLastExported(string supplierName)
        {
            var supplier = ExportedCollection.FindOne(Query.EQ("_id", supplierName));
            if (supplier == null)
            {
                return 0;
            }

            DateTime startdate = supplier.exportedDate;
            DateTime enddate = DateTime.Now;
            int  days = Convert.ToInt32((enddate -startdate).TotalDays);
            return days;
        }

        public string GetInfo(string supplier)
        {
            string result = "";

            try
            {

                var supplierName = GenerateOrderStatusCollection.Find(Query.EQ("_id", supplier));
                if (supplierName != null || supplierName.Count() > 0) { result = supplierName.First().status; }
            }
            catch
            {
               // result = "<small>Unexpected Error!</small>";
                //result = "<small><a href='javascript:;' onclick=\"if(confirm('Are you sure you want to export?')) {getPageToDiv('orderDiv_" + supplier + "', 'VeOrder/GenerateOrders?supplier=" + supplier + "'); CheckStatus('" + supplier + "');}\">Export</a></small>";
            }

            return result;
        }


        public void UpdateOrderedFromSupplier(string supplierOrderId, bool value)
        {

            GroupedItemsCollection.Update(Query.EQ("supplierOrderId", supplierOrderId), Update.Set("plus.orderedFromSupplier", value), UpdateFlags.Multi);

        }

        public void UpdateExportedBy(string supplierOrderId, string name)
        {

            GroupedItemsCollection.Update(Query.EQ("supplierOrderId", supplierOrderId), Update.Set("plus.exportedBy", name), UpdateFlags.Multi);

        }

        public void UpdateExportedDate(DateTime dateExported, string supplier) {

            ExportedCollection.Update(Query.EQ("_id", supplier), Update.Set("exportedDate", dateExported), UpdateFlags.Upsert);
        
        
        }

        public void UpdateStatus(string supplier, bool IsFinished = false)
        {
            if (IsFinished == true)
            {
                
                GenerateOrderStatusCollection.Update(Query.EQ("_id", supplier), Update.Set("status", "<small color='red'>FINISHED</small>"), UpdateFlags.Upsert);
               //GenerateOrderStatusCollection.Remove(Query.EQ("_id", supplier));
            }
            else
            {
                if (GenerateOrderStatusCollection.Find(Query.EQ("_id", supplier)).Count() == 0)
                {
                    GenerateOrderStatusCollection.Insert(new GenerateOrderStatusEntity { sellerId = supplier, status = "<font color='green'>Exporting... Please wait.</font>", filename = "" });
                }
            }
        }

        public void RemoveFinishedStatus() {

            GenerateOrderStatusCollection.Remove(Query.EQ("status", "<small color='red'>FINISHED</small>"));
        
        }

        public string GetDBUsed()
        {
            return amazon + "," + ebay + "," + reorder;
        }

        private static readonly Random _random = new Random();

        public ObjectId GenerateObjectId()
        {
            var timestamp = DateTime.UtcNow;
            var machine = _random.Next(0, 16777215);
            var pid = (short)_random.Next(0, 32768);
            var increment = _random.Next(0, 16777215);

            return new ObjectId(timestamp, machine, pid, increment);
        }

    }
}

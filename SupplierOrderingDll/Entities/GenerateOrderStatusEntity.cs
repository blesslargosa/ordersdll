﻿using MongoDB.Bson.Serialization.Attributes;

namespace SupplierOrderingDLL.Entities
{
	[BsonIgnoreExtraElements]
	public class GenerateOrderStatusEntity {
		[BsonId]
		public string sellerId { get; set; }
		public string status { get; set; }
		public string filename { get; set; }
	}
}

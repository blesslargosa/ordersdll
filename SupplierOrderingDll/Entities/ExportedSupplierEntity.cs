﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierOrderingDLL.Entities
{
    public class ExportedSupplierEntity
    {
        [BsonId]
        public string supplier { get; set; }
        public DateTime exportedDate { get; set; }
    }
}

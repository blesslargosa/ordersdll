﻿using DN_Classes.Entities;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierOrderingDLL.Entities
{
     [BsonIgnoreExtraElements]
    public class CalculatedProfitGeneratedEntity 
    {
         [JsonIgnore]
        public ObjectId _id { get; set; }
        public string Agentname { get; set; }
        public string SellerSku { get; set; }
        public string SupplierName { get; set; }
        public string Ean { get; set; }
        public string ArticleNumber { get; set; }
        public int VE { get; set; }
        public DateTime PurchasedDate { get; set; }
        public BsonDocument plus { get; set; }
        public object plus2 { get; set; }
        public PlatformCategory Platform { get; set; }
        public Income2 Income { get; set; }
        public Item2 Item { get; set; }
        public Tax2 Tax { get; set; }
        public Shipping2 Shipping { get; set; }
        public Packing2 Packing { get; set; }
        public Amazon AmazonOrder { get; set; }
        public Ebay EbayOrder { get; set; }

        [BsonIgnoreExtraElements]
        public class PlatformCategory
        {
            public string Category { get; set; }
            public double FeePercentage { get; set; }
            public double FeeinEuro { get; set; }
            public double VariableFixcost { get; set; }
            public double PaymentFeePercentage { get; set; }
            public double PaymentFeeEuro { get; set; }
        }  

         public class Income2
         {
             public int ShippingIncome { get; set; }
             public double NetShippingIncome { get; set; }
             public double SellPrice { get; set; }
         }

         [BsonIgnoreExtraElements]
         public class SimpleSize2
         {
             public int Length { get; set; }
             public int Width { get; set; }
             public int Height { get; set; }
             public int Weight { get; set; }
             public bool isValid { get; set; }
         }

         [BsonIgnoreExtraElements]
         public class Item2
         {
             public double NewBuyPrice { get; set; }
             public double DiscountedBuyprice { get; set; }
             public int Stock { get; set; }
             public double RiskCost { get; set; }
             public double Profit { get; set; }
             public double profitOverwrite { get; set; }
             public SimpleSize2 SimpleSize { get; set; }
         }

         public class Tax2
         {
             public int TaxRate { get; set; }
             public double ItemTax { get; set; }
             public double ShippingIncomeTax { get; set; }
         }

         public class Shipping2
         {
             public string ShippingName { get; set; }
             public double ShippingCosts { get; set; }
             public double PackingCosts { get; set; }
             public double HandlingCosts { get; set; }
         }

         public class Packing2
         {
             public int lenght { get; set; }
             public string name { get; set; }
             public double netPrice { get; set; }
             public double price { get; set; }
             public int weight { get; set; }
             public int width { get; set; }
         }

         public class Total2
         {
             public int CurrencyID { get; set; }
             public double Value { get; set; }
         }

         public class Transaction
         {
             public int currencyID { get; set; }
             public double Value { get; set; }
         }

       
        [BsonIgnoreExtraElements]
        public class Amazon
        {
            public string AmazonOrderId { get; set; }
            public string PurchaseDate { get; set; }
            public object LastUpdateDate { get; set; }
            public object OrderStatus { get; set; }
            public object FulfillmentChannel { get; set; }
            public object SalesChannel { get; set; }
            public object ShipServiceLevel { get; set; }
            public object ShippingAddress { get; set; }
            public object OrderTotal { get; set; }
            public object NumberOfItemsShipped { get; set; }
            public object NumberOfItemsUnshipped { get; set; }
            public object PaymentMethod { get; set; }
            public object MarketplaceId { get; set; }
            public object BuyerEmail { get; set; }
            public object BuyerName { get; set; }
            public object ShipmentServiceLevelCategory { get; set; }
            public object ShippedByAmazonTFM { get; set; }
            public object OrderType { get; set; }
            public object EarliestShipDate { get; set; }
            public object LatestShipDate { get; set; }
            public object ASIN { get; set; }
            public object SellerSKU { get; set; }
            public object OrderItemId { get; set; }
            public object Title { get; set; }
            public object QuantityOrdered { get; set; }
            public object QuantityShipped { get; set; }
            public object ItemPrice { get; set; }
            public object ShippingPrice { get; set; }
            public object GiftWrapPrice { get; set; }
            public object ItemTax { get; set; }
            public object ShippingTax { get; set; }
            public object GiftWrapTax { get; set; }
            public object ShippingDiscount { get; set; }
            public object PromotionDiscount { get; set; }
            public object ConditionNote { get; set; }
            public object ConditionId { get; set; }
            public object ConditionSubtypeId { get; set; }
            public object AgentId { get; set; }
        }

        [BsonIgnoreExtraElements]
        public class Ebay
        {
            public string _t { get; set; }
            public string OrderID { get; set; }
            public DateTime CreatedTime { get; set; }
            public string SellerUserID { get; set; }
            public string SellerEmail { get; set; }
            public object Total { get; set; }
            public List<TransactionArray> TransactionArray { get; set; }
        }
          [BsonIgnoreExtraElements]
        public class TransactionArray
        {
            public int QuantityPurchased { get; set; }
            public string OrderLineItemID { get; set; }
            public Transaction TransactionPrice { get; set; }
            public Item3 Item { get; set; }
        }

          public class Item3
          {
              public string ItemID { get; set; }
              public string Title { get; set; }
              public string SKU { get; set; }
          }
        

    

       
            
    }
}

﻿using DN_Classes.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierOrderingDLL.ReOrdering
{
    public class ReOrderFeedBackEntity
    {
        public List<string> SuccessfulReOrders { get; set; }
        public List<string> UnsuccessfulReOrders { get; set; }
        public List<string> CancelledOrders { get; set; }
        public List<CalculatedProfitEntity> ReoRderItems { get; set; }
    }
}

﻿using DN_Classes.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierOrderingDLL.ReOrdering.Interface
{
    public interface IReOrder
    {
        List<CalculatedProfitEntity> GetOrdersByEan(string ean);
        List<string> GetMultipleEansByOrderId(string orderId);
        IReOrder GetOrderItemPlatform(string ean, string orderId);
        CalculatedProfitEntity GetSpecificOrderByOrderId(string ean, string orderId);
        void UpdateReOrderedItem(string ean, string orderId, DateTime ReOrderDate);
        string GetAgentName(string orderId);
    }
}

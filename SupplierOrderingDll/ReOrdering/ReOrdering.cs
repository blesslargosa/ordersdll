﻿using DN_Classes.Entities;
using DN_Classes.Entities.WCF;
using DN_Classes.Products.Price;
using DN_Classes.Queries;
using MongoDB.Bson;
using MongoDB.Driver.Builders;
using SupplierOrderingDLL.ReOrdering.Interface;
using SupplierOrderingDLL.VeOrder.Classes;
using SupplierOrderingDLL.VeOrder.Interfaces;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using WhereToBuy;

namespace SupplierOrderingDLL.ReOrdering
{
    public class ReOrdering:DBCollections
    {
        public List<CalculatedProfitEntity> OrderItems { get; set; }
        public List<CalculatedProfitEntity> OrderItemsWithReOrderDate { get; set; }
        private List<IReOrder> platforms { get; set; }
        CalculatedProfitEntity currentOrder { get; set; }

        public ReOrdering(bool fetch) {

            this.OrderItems = GetReOrders();
            this.OrderItemsWithReOrderDate = GetOrderItemsWithReOrderDate();
        
        }

        public ReOrdering(){
            List<IReOrder> platforms = new List<IReOrder>();
            platforms.Add(new Amazon());
            platforms.Add(new Ebay());

            this.platforms = platforms;
        }

        private List<CalculatedProfitEntity> GetOrderItemsWithReOrderDate()
        {
            List<CalculatedProfitEntity> allreOrdereditems = new List<CalculatedProfitEntity>();
            var noOrderDates = this.OrderItems.Select(x => x).Where(x => x.plus != null && !x.plus.Contains("reOrderDate")).ToList();
            noOrderDates.ForEach(x => x.plus["reOrderDate"] = DateTime.MinValue);
            var hasOrderDates = this.OrderItems.Select(x => x).Where(x => x.plus != null && x.plus.Contains("reOrderDate")).ToList();
            hasOrderDates.ForEach(x => x.plus["reOrderDate"] = GetNewestDate(x.plus["reOrderDate"]));
            allreOrdereditems.AddRange(hasOrderDates);
            allreOrdereditems.AddRange(noOrderDates);
            allreOrdereditems = allreOrdereditems.OrderByDescending(x => DateTime.Parse(((DateTime)x.plus["reOrderDate"]).ToString(CultureInfo.CurrentCulture))).ToList();

            return allreOrdereditems;
        }
        public List<CalculatedProfitEntity> GetReOrders()
        {

            return new ReOrderItemsQueries().GetReOrders();

        }

        public DateTime GetNewestDate(BsonValue dates)
        {
            if (dates.IsBsonArray)
            {

                return DateTime.Parse(dates.AsBsonArray.OrderByDescending(x => DateTime.Parse(x.ToString())).First().ToString());
            }

            else if (dates != BsonNull.Value && dates != null)
            {
                return (DateTime)dates;
            }

            else
            {
                return DateTime.MinValue;
            }
        }

        public void ReOrderItemByEan(string ean, DateTime reOrderDate, string note)
        {
            CheapestPrice getCheapestSupplier = new CheapestPrice();
            BuyPriceRow row = new BuyPriceRow(ean);

            try { row = getCheapestSupplier.GetCheapestPriceByEanExcludedJTL(ean, ""); }
            catch { }

            new ReOrderItemsCommands().ImportReOrderItemByEan(ean, row, reOrderDate, note);
            
        }

        public void ReOrderItemByOrderId(string ean, string orderId, DateTime reOrderDate, string reOrderId, string note, IReOrder platform)
        {
            this.currentOrder = platform.GetSpecificOrderByOrderId(ean, orderId);

            int multiplier = ExtractMultiplier(this.currentOrder.SellerSku);

            //if (multiplier >= 1)
            //{
            //    for (int x = 1; x <= multiplier; x++)
            //    {
                    DoImportUpdate(ean, orderId, reOrderDate, reOrderId, note, platform, this.currentOrder);
            //    }
            //}
            
        }

        private void DoImportUpdate(string ean, string orderId, DateTime reOrderDate, string reOrderId, string note, IReOrder platform, CalculatedProfitEntity item)
        {
            ImportReOrderItem(item, reOrderDate, reOrderId, GetWhereToBuySupplier(ean), note);
            platform.UpdateReOrderedItem(ean, orderId, reOrderDate);
        }

        

        private int ExtractMultiplier(string sku)
        {
            if(!String.IsNullOrEmpty(sku)){

                try { return Convert.ToInt32(sku.Split('-')[3].ToString()); }
                catch { return 1; }
            }

            return 1;
        }

        private void ImportReOrderItem(CalculatedProfitEntity item, DateTime reOrderDate,string reOrderId, string supplier, string note)
        {
            new ReOrderItemsCommands().ImportReOrderItem(item, reOrderDate,reOrderId, supplier, note);
        }

        

        public string GetSupplierOrderId(CalculatedProfitEntity order)
        {
            if (order.plus.Contains("supplierOrderId"))
            {
                return order.plus["supplierOrderId"].ToString();
            }

            return "";
        }

        public string GetWhereToBuySupplier(string ean)
        {

            CheapestPrice getCheapestSupplier = new CheapestPrice();
            BuyPriceRow row = new BuyPriceRow(ean);
            try { row = getCheapestSupplier.GetCheapestPriceByEanExcludedJTL(ean, ""); }
            catch { }

            return row.suppliername.ToString();

        }

        public ReOrderFeedBackEntity ProcessReOrder(MatchCollection matches, IReOrder pf, DateTime reOrderDate, string note)
        {
            List<string> cancelledOrders = new List<string>();
            List<string> successfulReorders = new List<string>();
            List<string> unsuccessfulReorders = new List<string>();

            foreach (var orderId in matches)
            {
                bool cancelled = new WareHouseQueries().checkIfOrderIsCancelled(orderId.ToString());
                if (cancelled == true)
                {
                    cancelledOrders.Add(orderId.ToString());
                }
                else
                {
                    //get ean/s
                    List<string> orderedEans = pf.GetMultipleEansByOrderId(orderId.ToString());

                    if (orderedEans.Count() > 0)
                    {
                        //reorder every item in an order
                        orderedEans.ForEach(x => ReOrderItemByOrderId(x, orderId.ToString(), reOrderDate, reOrderDate.Ticks.ToString(),note, pf));
                        successfulReorders.Add(orderId.ToString());
                    }
                    else
                    {
                        unsuccessfulReorders.Add(orderId.ToString());
                    }
                }
            }

            ReOrderFeedBackEntity rf = new ReOrderFeedBackEntity
            {

                SuccessfulReOrders = successfulReorders,
                UnsuccessfulReOrders = unsuccessfulReorders,
                CancelledOrders = cancelledOrders


            };

            return rf;
        }

        public Dictionary<string, List<EanSupplierEntity>> ProcessreOrderCustomTool(string amazonOrderId, IReOrder pf, DateTime reOrderDate, string note) 
        {

            Dictionary<string, List<EanSupplierEntity>> FeedbackEans = new Dictionary<string, List<EanSupplierEntity>>();
            List<string> orderedEans = new List<string>();
            List<EanSupplierEntity> eanCheapestSupplier = new List<EanSupplierEntity>();
            string feedback = "";


            bool cancelled = new WareHouseQueries().checkIfOrderIsCancelled(amazonOrderId);
            if (cancelled == true)
            {
                feedback = "CANCELLED";
            }
            else
            {
               orderedEans = pf.GetMultipleEansByOrderId(amazonOrderId);

                if (orderedEans.Count() > 0)
                {
                    //reorder every item in an order
                    orderedEans.ForEach(x => ReOrderItemByOrderId(x, amazonOrderId, reOrderDate, reOrderDate.Ticks.ToString(), note, pf));
                    
                    //get cheapest supplier each ean
                    foreach (var ean in orderedEans)
                    {
                        EanSupplierEntity eansupp = new EanSupplierEntity { ean = ean, supplier = GetWhereToBuySupplier(ean), multiplier = ExtractMultiplier(this.currentOrder.SellerSku) };
                        eanCheapestSupplier.Add(eansupp);
                    }
                    feedback = "SUCCESSFUL";
                }
                else
                {
                    feedback = "UNSUCCESSFUL";
                }
            }

            FeedbackEans.Add(feedback, eanCheapestSupplier);

            return FeedbackEans; 

        
        }
        public void DeleteReOrderedItem(ObjectId id)
        {
            ReOrderCollection.Remove(Query.EQ("_id", id));
        }
    }
}

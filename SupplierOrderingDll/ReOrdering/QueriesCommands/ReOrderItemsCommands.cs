﻿
using DN_Classes.Entities;
using DN_Classes.Products.Price;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierOrderingDLL.ReOrdering
{
    public class ReOrderItemsCommands : DBCollections
    {

        public void ImportReOrderItem(CalculatedProfitEntity item, DateTime reOrderDate, string reOrderId,string supplier, string note)
        {
            item.plus = new BsonDocument();

            item.SupplierName = supplier;
           
            item._id = new Others().GenerateObjectId();
            item.plus["reOrderId"] = reOrderId;
            item.plus["reOrderDate"] = reOrderDate;
            item.plus["note"] = note;
            ReOrderCollection.Insert(item);


        }



        public ObjectId ImportReOrderItemByEan(string ean, BuyPriceRow row, DateTime reOrderDate, string note)
        {

            ObjectId newId = new Others().GenerateObjectId();
            ItemEntity item = new ItemEntity
            {
                NewBuyPrice = row.price
            };

            BsonDocument plus = new BsonDocument();
            plus["reOrderDate"] = reOrderDate;
            plus["note"] = note;

            CalculatedProfitEntity newItem = new CalculatedProfitEntity
            {
                _id = newId,
                Ean = ean,
                SupplierName = row.suppliername,
                ArticleNumber = row.artnr,
                Item = item,
                VE = row.packingUnit,
                PurchasedDate = reOrderDate,
                plus = plus
            };



            ReOrderCollection.Insert(newItem);

            return newId;
        }

        public void UpdateReOrderItemByEan(ObjectId id, BuyPriceRow row, string note, DateTime reOrderDate)
        {
            ReOrderCollection.Update(Query.And(Query.EQ("_id", id), Query.EQ("SupplierName", row.suppliername)),
            Update.Set("plus.reOrderDate", reOrderDate)
            .Set("plus.note", note));
        }

        public void DeleteReOrderedItem(ObjectId id)
        {

            ReOrderCollection.Remove(Query.EQ("_id", id));

        }
    }
}

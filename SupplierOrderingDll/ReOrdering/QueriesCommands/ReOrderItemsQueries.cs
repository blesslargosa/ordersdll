﻿
using DN_Classes.Entities;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierOrderingDLL.ReOrdering
{
    public class ReOrderItemsQueries : DBCollections
    {
        public List<CalculatedProfitEntity> GetReOrders() {

           
            return ReOrderCollection.Find(Query.GTE("plus.reOrderDate",DateTime.Now.AddDays(-90))).ToList();
        
        }

        public List<CalculatedProfitEntity> GetItemsByOrderId(string orderId)
        {

            return ReOrderCollection.Find(Query.EQ("AmazonOrder.AmazonOrderId", orderId)).ToList();
        
        }

        public bool IsItemReOrdered(string orderId)
        {

            bool hasReOrdered = false;
            var orderedItems = ReOrderCollection.Find(Query.EQ("AmazonOrder.AmazonOrderId", orderId)).ToList();

            if (orderedItems.Count > 1)
            {
                hasReOrdered = true;

            }
            return hasReOrdered;
        }
    }
}

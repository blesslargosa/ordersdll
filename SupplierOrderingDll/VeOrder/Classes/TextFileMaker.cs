﻿using DN_Classes.Supplier;
using SupplierOrderingDLL.OrderPurchaseFormatting;
using SupplierOrderingDLL.OrderPurchaseFormatting.ExportingFormats;
using SupplierOrderingDLL.OrderPurchaseFormatting.FormatIndicators;
using SupplierOrderingDLL.OrderPurchaseFormatting.FormattingFactory;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierOrderingDLL.VeOrder.Classes
{
    public class TextFileMaker
    {
        public void CreateFile(string filePath)
        {
            if (File.Exists(filePath))
            {

                File.Delete(filePath);
            }
            File.Create(filePath).Close();
        }

        public void CreateDirectory(string path)
        {
            if (Directory.Exists(path))
                return;
            Directory.CreateDirectory(path);
        }

        internal string WriteFile(string suppliername, List<GroupedItemsEntity> orderItems, string supplierOrderId)
        {
            
            string path = AppDomain.CurrentDomain.BaseDirectory + "OrderFiles\\";
            CreateDirectory(path);
            string filepath = CreateFilePath(supplierOrderId, path, "");
            CreateFile(filepath);

            WriteToFileWithFormatting(suppliername,orderItems,filepath);

            return filepath;
        }

        internal string WriteFile_OrderHistory(string supplierOrderId, List<GroupedItemsEntity> orderItems) {

            string path = AppDomain.CurrentDomain.BaseDirectory + "OrderFiles\\";
            CreateDirectory(path);
            string filepath = CreateFilePath(supplierOrderId, path, "_H");
            CreateFile(filepath);

            WriteToFile(filepath,orderItems);

            return filepath;
        
        
        }

        private void WriteToFile(string filepath, List<GroupedItemsEntity> orderItems)
        {
            
            List<string> lines = new List<string>();
            lines.Add("ean\tartnr\tsku\tvesize\tbuyprice\tmultiplier\tstock\tordereditems\torderedve\tveprice");
            foreach (var item in orderItems) {

                List<string> line = new List<string>();

                line.Add(item.ean);
                line.Add(item.supplierSku);
                line.Add(item.sellerSKU);
                line.Add(item.packingUnitSize.ToString());
                line.Add(item.buyPrice.ToString("0.0"));
                line.Add(item.multiplier.ToString());
                line.Add(item.itemsInStock.ToString());
                line.Add(item.numberOfOrderedItems.ToString());
                line.Add(item.itemsToOrderFomSupplier.ToString());
                line.Add(item.packingUnitsToOrderFromSupplier.ToString());
                line.Add(item.totalPackingUnitsPrice.ToString("0.0"));


                string oneline = String.Join("\t", line);

                lines.Add(oneline);
            }

            File.WriteAllText(filepath, String.Join("\r\n", lines));

        }
        private void WriteToFileWithFormatting(string supplierName, List<GroupedItemsEntity> groupedorders, string filename)
        {
            
            SupplierDB supplierDB = new SupplierDB();
            SupplierEntity supplierEntity = supplierDB.GetSupplier(supplierName.ToUpper());
            IFormatIndicator formatIndicator = new PlusSectionIndicator(supplierEntity.plus);

            IFormatFactory formatFactory = new ExportFormatFactoryBasic(formatIndicator);
            IExportFormat exportFormat = formatFactory.GetExportFormat();

            exportFormat.Prepare(groupedorders, filename);

            ISupplierOrderExport supplierOrderExport = new SupplierOrderExportBasic();
            supplierOrderExport.Write(exportFormat);
           
        }

        private string CreateFilePath(string supplierOrderId, string path, string addon)
        {
            return path + supplierOrderId + addon + ".txt";
        }

       
    }
}

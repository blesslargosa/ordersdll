﻿using DN_Classes.Entities;
using DN_Classes.Supplier;
using MongoDB.Bson;
using MongoDB.Driver.Builders;
using SupplierOrderingDLL.VeOrder.Classes.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SupplierOrderingDLL.VeOrder.Classes
{
    public class GroupItems
    {

        
        public List<GroupedItemsEntity> groupedItemsByEanSupplier { get; set; }
        public List<GroupedItemsEntity> groupedItemsBySpecificSupplier { get; set; }
        public List<GenericEntity> orderItems { get; set; }
        public List<SupplierLevelGroupedEntity> groupedItemsBySupplier { get; set; }
        public List<SupplierLevelGroupedEntity> groupedItemsBySupplierIncludingNoOrders { get; set; }

        int itemsToOrderFromsupplier { get; set; }
        int packingUnitsToOrderFromSupplier { get; set; }

        public GroupItems() { }
        public GroupItems(List<GenericEntity> orderItems) {

            this.orderItems = orderItems;
            this.groupedItemsByEanSupplier = GetGroupedItemsByEanSupplier();
            this.groupedItemsBySupplier = GetGroupedItemsBySupplier();

            var orders = GetGroupedItemsSuppliersWithoutOrders();
            orders.Sort((x, y) => x.VePrice.CompareTo(y.VePrice));
            orders.Reverse();

            this.groupedItemsBySupplierIncludingNoOrders = orders;

            
        }

        private List<SupplierLevelGroupedEntity> GetGroupedItemsBySupplier()
        {
            Others other = new Others();
            return groupedItemsByEanSupplier.GroupBy(x => x.supplier).Select(y => new SupplierLevelGroupedEntity { 
            
                supplier = y.Key,
                VePrice = y.Sum(x => x.totalPackingUnitsPrice),
                items = y.ToList(),
                minPurchasedDate = y.AsParallel().Min(z =>z.minPurchasedDate),
                exported16hr = other.CheckExportStatus(y.Key),
                lastExportedDate = other.GetLastExportedDateTime(y.Key),
                dayspassedlastexported = other.GetCountDaysPassedLastExported(y.Key),
                status = other.GetInfo(y.Key)

            }).ToList();
        }

       
        public List<GroupedItemsEntity> GetGroupedItemsBySpecificSupplier(string supplier) { 
        
           return  this.groupedItemsBySupplier.AsParallel().Where(x => x.supplier.ToLower().Equals(supplier.ToLower())).Select( y => y.items).First().ToList();
            
        }

        private List<SupplierLevelGroupedEntity> GetGroupedItemsSuppliersWithoutOrders()
        {
            
            Others otherqueries = new Others();
            List<string> allsupplierswithOrders = this.groupedItemsBySupplier.AsParallel().Select(x => x.supplier.ToLower()).ToList();

                   //ParallelOptions po = new ParallelOptions();
                   // po.MaxDegreeOfParallelism = 5;

                    //Parallel.ForEach(otherqueries.GetSuppliers(), po, (supplier) =>
                    //{
            foreach (var supplier in otherqueries.GetSuppliers())
            {
                if (!allsupplierswithOrders.Contains(supplier._id.ToLower()))
                {

                    bool exportstatus = otherqueries.CheckExportStatus(supplier._id);
                    int days = otherqueries.GetCountDaysPassedLastExported(supplier._id);
                    DateTime lastexported = otherqueries.GetLastExportedDateTime(supplier._id);
                    SupplierLevelGroupedEntity noOrder = new SupplierLevelGroupedEntity { supplier = supplier._id, items = null, exported16hr = exportstatus, dayspassedlastexported = days, lastExportedDate = lastexported };

                    this.groupedItemsBySupplier.Add(noOrder);
                }
            }
                    
                    //});
         

            return this.groupedItemsBySupplier;
        }

      

        private List<GroupedItemsEntity> GetGroupedItemsByEanSupplier()
        {
            var orders =  orderItems.GroupBy(x => new { x.ean, x.supplier })

                .Select(y => new GroupedItemsEntity
                {

                    ean = y.Key.ean,

                    supplier = y.Key.supplier,

                    supplierSku = y.First().artnr,

                    packingUnitSize = y.First().veSize,

                    buyPrice = y.First().price,

                    singleOrders = y.Select(a => a.singleorder).ToList(),

                    multiplier = y.First().multiplier,

                    itemsInStock = y.First().warehouseStock,

                    numberOfOrderedItems = y.AsParallel().Select(a => a.singleorder).ToList().Count(),

                    itemsToOrderFomSupplier = GetOrderItemsWithMultiplierStock(y.Select(a => a.singleorder).ToList().Count(), y.First().multiplier, y.First().warehouseStock),

                    packingUnitsToOrderFromSupplier = GetVeToOrder(this.itemsToOrderFromsupplier, y.First().veSize),

                    minPurchasedDate = y.AsParallel().Min(a => a.minPurchasedDate),

                    totalPackingUnitsPrice = y.First().price * this.packingUnitsToOrderFromSupplier,

                    sellerSKU = y.First().sku,

                    plus = AppendPlatform(y.First().platform, y.First().orderIdObjectId)

                }).ToList();

            orders.Sort((x, y) => x.minPurchasedDate.CompareTo(y.minPurchasedDate));
            orders.Reverse();

            return orders;
        }

        private BsonDocument AppendPlatform(string platform, string orderIdObjectId)
        {
            BsonDocument bson = new BsonDocument();
            bson["platform"] = platform;
            bson["orderId"] = orderIdObjectId;

            return bson;
        }

        private string ExtractAsin(string sku)
        {
            string skuPattern = "\b[A-Z0-9]{1,15}[-][A-Z0-9]{10}[-][0-9]{13}\b";

            Match matchSku = Regex.Match(sku, skuPattern);

            if (matchSku.Success == true)
            {
                try { return sku.Split('-')[1]; }
                catch { return sku; }
            }
            else
            {
                return sku;
            }
        }

        private string ExtractPrefix(string sku)
        {
            string skuPattern = "\b[A-Z0-9]{1,15}[-][A-Z0-9]{10}[-][0-9]{13}\b";

            Match matchSku = Regex.Match(sku, skuPattern);

            if (matchSku.Success == true)
            {
                try { return sku.Split('-')[0]; }
                catch { return sku; }
            }
            else
            {
                return sku;
            }
        }

        private int GetOrderItemsWithMultiplierStock(int totalordereditems, int multiplier, int warehouseStock)
        {
            this.itemsToOrderFromsupplier = (totalordereditems * multiplier) - warehouseStock;

            return this.itemsToOrderFromsupplier;
        }

        public int GetVeToOrder(int netItems, int veSize)
        {
            if (veSize == 0) { veSize = 1; }
            if (netItems <= 0) { this.packingUnitsToOrderFromSupplier = 0; return 0; }
            if (veSize > netItems) { this.packingUnitsToOrderFromSupplier = 1; return 1; }
            

            int i = 0;
            if (netItems % veSize == 1)
            {
                i = netItems / veSize + 1;
                this.packingUnitsToOrderFromSupplier = i;
                return i;
            }

            i = netItems / veSize;
            this.packingUnitsToOrderFromSupplier = i;
            return i;
        }
    }
}

﻿using DN_Classes.Entities;
using SupplierOrderingDLL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierOrderingDLL.VeOrder.Classes
{
    public class GatherOrderItems
    {
        public List<CalculatedProfitEntity> allPlatformItemsNotGrouped { get; set; }
        public List<GenericEntity> allGenericFieldItems { get; set; }
        public List<CalculatedProfitEntity> OrdersByEan { get; set; }

        public GatherOrderItems() {

            //this.allPlatformItemsNotGrouped = GetAllPlatformItemsNotGrouped();
            this.allGenericFieldItems = GetllGenericFieldItems();
        
        }

        public GatherOrderItems(string ean) {

            this.OrdersByEan = GetOrdersByEan(ean);
           
        
        }

        private List<CalculatedProfitEntity> GetOrdersByEan(string ean)
        {
            List<CalculatedProfitEntity> calculatedItemsByEan = new List<CalculatedProfitEntity>();
            calculatedItemsByEan.AddRange(new Amazon().GetOrdersByEan(ean));
            calculatedItemsByEan.AddRange(new Ebay().GetOrdersByEan(ean));
            calculatedItemsByEan.AddRange(new ReOrder().GetOrdersByEan(ean));

            return calculatedItemsByEan;
        }

       

        private List<GenericEntity> GetllGenericFieldItems()
        {
            List<GenericEntity> allGenerics = new List<GenericEntity>();
            allGenerics.AddRange(new Amazon(true).GenericFields);
            allGenerics.AddRange(new Ebay(true).GenericFields);
            allGenerics.AddRange(new ReOrder(true).GenericFields);

            return allGenerics;
        }

        private List<CalculatedProfitEntity> GetAllPlatformItemsNotGrouped()
        {
            List<CalculatedProfitEntity> all = new List<CalculatedProfitEntity>();
            all.AddRange(new Amazon(true).Orders);
            all.AddRange(new Ebay(true).Orders);
            all.AddRange(new ReOrder(true).Orders);

            return all;
        }
    }
}

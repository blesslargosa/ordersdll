﻿using DN_Classes.Logger;
using MongoDB.Bson;
using SupplierOrderingDLL.VeOrder.Classes;
using SupplierOrderingDLL.VeOrder.Classes.Entities;
using SupplierOrderingDLL.VeOrder.Interfaces;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierOrderingDLL.VeOrder
{
    public class ProcessOrderItems:DBCollections
    {
        private List<Classes.GroupedItemsEntity> groupedItemsBysupplierOrderIdEan;
   
       
        private string supplierOrderId { get; set; }
        private string user { get; set; }
        public DateTime dateExported { get; set; }
        public string supplier { get; set; }
        public string vePrice { get; set; }
        private string LogFileName_Info { get; set; }
        public List<string> listOfLogs_Info = new List<string>();
        public List<string> listOfLogs_InfoSummary = new List<string>();
        public List<string> listOfLogs_Error = new List<string>();
       


        public ProcessOrderItems(List<Classes.GroupedItemsEntity> groupedItemsBysupplierOrderIdEan, string supplier,string user, string vePrice)
        {
            
            this.dateExported =  DateTime.Now;
            var dateTick = this.dateExported.ToString("yyyy-MM-dd---HH-mm-ss-fff");
            this.supplier = supplier;
            this.supplierOrderId = CreateOrderId(supplier, dateTick);
            this.groupedItemsBysupplierOrderIdEan = groupedItemsBysupplierOrderIdEan;
            this.user = user;
            this.vePrice = vePrice;
           

            listOfLogs_Info.Add("\r\n\nSUPPLIERORDERID : " + supplierOrderId);
            listOfLogs_InfoSummary.Add("\r\n\nSUPPLIERORDERID : " + supplierOrderId);
            listOfLogs_InfoSummary.Add("EXPORTED DATE : " + dateTick);
            listOfLogs_InfoSummary.Add("VEPRICE : " + vePrice);
            listOfLogs_InfoSummary.Add("TOTAL GROUPEDORDERS : " + groupedItemsBysupplierOrderIdEan.Count);


            
                DoProcess();
                SaveOrderedItems();
                UpdatePlus();
                WriteLogs();
            
        }

        private void WriteLogs()
        {
            string LogFileName_Errors = AppDomain.CurrentDomain.BaseDirectory + "OrderFiles\\_GenerateOrdersErrors.txt";
            string LogFileName_Info = AppDomain.CurrentDomain.BaseDirectory + "OrderFiles\\_GenerateOrdersInfo_ProcessedOrders.txt";
            string LogFileName_InfoSummary = AppDomain.CurrentDomain.BaseDirectory + "OrderFiles\\_GenerateOrdersInfoSUMMARY.txt";
            
            File.AppendAllText(LogFileName_Info, String.Join("\r\n", listOfLogs_Info));
            File.AppendAllText(LogFileName_Errors, String.Join("\r\n", listOfLogs_Error));
            File.AppendAllText(LogFileName_InfoSummary, String.Join("\r\n", listOfLogs_InfoSummary));

        }

        private string CreateOrderId(string suppliername, string dateTick)
        {
            return suppliername + "_" + dateTick;
        }

        private void UpdatePlus()
        {
            try
            {
                Others other = new Others();
                other.UpdateExportedBy(this.supplierOrderId, this.user);
                other.UpdateExportedDate(this.dateExported, this.supplier);
                other.UpdateStatus(this.supplier, true);
            }
            catch (Exception ex) { listOfLogs_Error.Add(ex.ToString()); listOfLogs_Error.Add(this.supplierOrderId); }
        }

        public void DoProcess(){
            
            /*log*/listOfLogs_Info.Add("PROCESSED ORDERS (WITH PLATFORM) : ");
           
            int totalSingleOrdersToProcess = 0;
            int SinlgeOrdersProcessed = 1;
            int UnSuccessfulProcessed = 0;
            int SuccessfulProcessed = 0;

            List<IPlatforms> platforms = new Platforms().GetAllPlatforms();

           
                //update calculatedProfitDB plus for ordered indicator
                foreach (IPlatforms pl in platforms)
                {
                    string pf = pl.GetType().Name.ToLower();
                    try{
                    

                    var objectIds = this.groupedItemsBysupplierOrderIdEan
                        .SelectMany(x => x.singleOrders).Where(x => x.plus["platform"].AsString.ToLower().Equals(pf))
                        .Select(x => x._id).ToList();

                   
                    totalSingleOrdersToProcess = totalSingleOrdersToProcess + objectIds.Count();

                    if (objectIds.Count > 0)
                    {
                        
                       
                        foreach (var id in objectIds)
                        {
                           
                            try
                            {
                                IPlatforms platform = pl;
                                platform.UpdateCalculatedOrderPlus(id, this.supplierOrderId, this.dateExported);
                                /*log*/listOfLogs_Info.Add(id.ToString() + " " + pf + "#" + SinlgeOrdersProcessed);
                                SuccessfulProcessed++;
                                
                            }
                            catch (Exception ex)
                            {
                                  UnSuccessfulProcessed++;
                                 /*log*/listOfLogs_Error.Add("---------------");
                                 /*log*/listOfLogs_Error.Add(this.supplierOrderId);
                                 /*log*/listOfLogs_Error.Add(id.ToString() + " " + pf + "#" + SinlgeOrdersProcessed);
                                 /*log*/listOfLogs_Error.Add(ex.ToString());
                                 
                            }

                            SinlgeOrdersProcessed++;
                            
                        }

                       
                    }

                    }
                    catch (Exception ex)
                    {
                        /*log*/listOfLogs_Error.Add("---------------");
                        /*log*/listOfLogs_Error.Add(this.supplierOrderId + " " + pf + "#" + SinlgeOrdersProcessed);
                        /*log*/listOfLogs_Error.Add(ex.ToString());

                    }

                }


                /*log*/listOfLogs_InfoSummary.Add("TOTAL SINGLE ORDERS TO PROCESS : " + totalSingleOrdersToProcess.ToString());
                /*log*/listOfLogs_InfoSummary.Add("TOTAL SINGLE ORDERS PROCESSED : " + (SinlgeOrdersProcessed - 1).ToString());
                /*log*/listOfLogs_InfoSummary.Add("SUCCESSFULLY PROCESSED : " + SuccessfulProcessed.ToString());
                /*log*/listOfLogs_InfoSummary.Add("UNSUCCESSFULLY PROCESSED : " + UnSuccessfulProcessed.ToString());
        }

        public void SaveOrderedItems()
        {
            try
            {
                //save grouped items to groupedCollection
                foreach (var item in this.groupedItemsBysupplierOrderIdEan)
                {
                    item.supplierOrderId = this.supplierOrderId;
                    item.plus["OrderTotalVePrice"] = Double.Parse(this.vePrice, CultureInfo.InvariantCulture);
                    item.plus["DateExported"] = this.dateExported;
                    if (!item.plus.Contains("OrderedExpectedArrived"))
                    {
                        item.plus.Add("OrderedExpectedArrived", new BsonDocument { { "expected", item.packingUnitsToOrderFromSupplier },{"ordered",item.packingUnitsToOrderFromSupplier} });
                    }
                    else
                    {
                        item.plus["OrderedExpectedArrived"]["expected"] = item.packingUnitsToOrderFromSupplier;
                        item.plus["OrderedExpectedArrived"]["ordered"] = item.packingUnitsToOrderFromSupplier;
                    }
                    GroupedItemsCollection.Insert(item);
                    
                }

                
            }
            catch (Exception ex){

                listOfLogs_Error.Add(ex.ToString());
                listOfLogs_Error.Add(this.supplierOrderId);
            
            }
        }

        
        
    }
}

﻿using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using SupplierOrderingDLL.VeOrder.Classes.Entities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierOrderingDLL.VeOrder.Classes
{
   public class OrderHistory:DBCollections
    {

        public  List<GroupedItemsEntity> GeneratedGroupedItemsBySupplier { get; set; }
        public List<GroupedSupplierOrderIdItems> GeneratedGroupedItemsBySupplierOrderId { get; set; }


        public OrderHistory(string suppliername, string dateOption)
        {


           this.GeneratedGroupedItemsBySupplier = GetGeneratedGroupedItems(suppliername);
           this.GeneratedGroupedItemsBySupplierOrderId = GetGroupedItemsBySupplierOrderId(dateOption);
           
       }

       public OrderHistory() { }

       public List<GroupedItemsEntity> GetGeneratedGroupedItems(string suppliername)
       {


           return GroupedItemsCollection.Find(Query.And(Query.EQ("supplier", suppliername))).SetFields(Fields
                       .Include("supplierOrderId")
                       .Include("ean")
                       .Include("supplier")
                       .Include("supplierSku")
                       .Include("packingUnitSize")
                       .Include("buyPrice")
                       .Include("totalPackingUnitsPrice")
                       .Include("packingUnitsToOrderFromSupplier")
                       .Include("itemsInStock")
                       .Include("numberOfOrderedItems")
                       .Include("singleOrders.Ean")
                       .Include("itemsToOrderFomSupplier")
                       .Include("plus"))
               .ToList();
          
       }

       public List<GroupedSupplierOrderIdItems> GetGroupedItemsBySupplierOrderId(string dateOption)
       {

           var groupedOrdersBySupplierOrderId = this.GeneratedGroupedItemsBySupplier.GroupBy(x => x.supplierOrderId).AsParallel()

               .Select(y => new GroupedSupplierOrderIdItems { 
               
               supplierOrderId = y.Key,
               groupedOrders = y.ToList(),
               dateExported =  ConvertToDate(y.Key),
               orderedFromSupplier = CheckOrdered(y.ToList().First()),
               exportedBy =  CheckExportedBy(y.ToList().First()),
               orderTotalVePrice = GetOrderTotalPrice(y.First().plus),
               billdetails = GetBillDetails(y.ToList())
               }).ToList();

           
           int daysbefore = -1 * Convert.ToInt32(dateOption);
           if (dateOption == "0") {
               return groupedOrdersBySupplierOrderId;
           }else{
           return groupedOrdersBySupplierOrderId.Where(x => x.dateExported >= DateTime.Now.AddDays(daysbefore)).ToList();
           }
       }

       public List<GroupedItemsEntity> GetOrdersBySupplierOrderId(string supplierOrderId)
       {

           return GroupedItemsCollection.Find(Query.EQ("supplierOrderId", supplierOrderId)).ToList();

       }

       public List<GroupedItemsEntity> GetOrdersBySupplierOrderIdByEan(string supplierOrderId, string ean)
       {

           return GroupedItemsCollection.Find(Query.And(Query.EQ("supplierOrderId", supplierOrderId),Query.EQ("ean",ean))).ToList();

       }

       private BillDetails GetBillDetails(List<GroupedItemsEntity> list)
       {
           var billdetails = list.Where(y => y.plus != null && y.plus.Contains("BillDetails")).ToList();

           if (billdetails.Count > 0)
           {
               var plusBillDetails = billdetails.Select(x => x.plus["BillDetails"]).First();

               BillDetails b = new BillDetails { billFileName = plusBillDetails["billFileName"].AsString, billPrice = plusBillDetails["billPrice"].AsString, billComment = plusBillDetails["billComment"].AsString };

               return b;
           }

           return null;
       
       }

       private double GetOrderTotalPrice(BsonDocument bson)
       {
           try
           {
               if (bson.Contains("OrderTotalVePrice"))
               {

                   return bson["OrderTotalVePrice"].AsDouble;
               }
           }
           catch { return 0; }
           return 0;
       }

       private DateTime ConvertToDate(string supplierOrderId)
       {
           try
           {
               return DateTime.ParseExact(supplierOrderId.Split('_')[1], "yyyy-MM-dd---HH-mm-ss-fff", CultureInfo.InvariantCulture);
           }
           catch
           {
               return new DateTime(long.Parse(supplierOrderId.Split('_')[1]));
           }
       }

       private bool CheckOrdered(GroupedItemsEntity groupedOrders)
       {

           if (groupedOrders.plus != null && groupedOrders.plus.Contains("orderedFromSupplier"))
           { return groupedOrders.plus["orderedFromSupplier"].AsBoolean; }
           else { return false; }
       }

       private string CheckExportedBy(GroupedItemsEntity groupedOrders)
       {

           if (groupedOrders.plus != null && groupedOrders.plus.Contains("exportedBy"))
           { return groupedOrders.plus["exportedBy"].AsString; }
           else { return ""; }
       }

       public void SaveScannedEans(List<string> eans, string supplierOrderId)
       {
           var groupedbyEans = eans.GroupBy(x => x).Select(y => new { ean = y.Key, count = y.Count() }).ToList();

           ParallelOptions po = new ParallelOptions();
           po.MaxDegreeOfParallelism = 5;
           Parallel.ForEach(groupedbyEans, po, (ean) => {

               lock (new object()) {

                   InsertArrived(supplierOrderId, ean.count, ean.ean);
               }           
           
           });
       }

       public void SetBillDetails(string fileName, string supplierOrderId, string comment, string billPrice)
       {
           GroupedItemsCollection.Update(Query.EQ("supplierOrderId", supplierOrderId),
               Update.Set("plus.BillDetails.billFileName", fileName)
               .Set("plus.BillDetails.billPrice", billPrice)
               .Set("plus.BillDetails.billComment", comment), UpdateFlags.Multi);

       }

       public void CalculateSaveMissingItems(List<string> eans, string supplierOrderId)
       {
           var groupedbyEansArrived = eans.GroupBy(x => x).Select(y => new { ean = y.Key, count = y.Count() }).ToList();

           ParallelOptions po = new ParallelOptions();
           po.MaxDegreeOfParallelism = 5;
           Parallel.ForEach(groupedbyEansArrived, po, (ean) =>
           {

               lock (new object())
               {

                   var order = GetOrdersBySupplierOrderIdByEan(supplierOrderId, ean.ean);

                   int missing = GetOrdered(order) - ean.count;

                  UpdateMissing(supplierOrderId, missing, ean.ean);
                 
                   
               }

           });

       }

       private int GetOrdered(List<GroupedItemsEntity> orders)
       {
           return orders.Where(x => CheckOrderedExpectedArrived(x.plus) == true).Select(x => x.plus["OrderedExpectedArrived"]["ordered"].AsInt32).First();

       }

     

       public int GetExpected(List<GroupedItemsEntity> orders)
       {

           return orders.Where(x => CheckOrderedExpectedArrived(x.plus) == true).Select(x => x.plus["OrderedExpectedArrived"]["expected"].AsInt32).First();

       
       }
       private bool CheckOrderedExpectedArrived(BsonDocument plus)
       {
          if( plus.Contains("OrderedExpectedArrived")){


              return true;
          }

          return false;
       }

       public void InsertArrived(string supplierOrderId, int numberofArrivedItem, string ean) {

           GroupedItemsCollection.Update(Query.And(Query.EQ("supplierOrderId", supplierOrderId), Query.EQ("ean", ean)), Update.Set("plus.OrderedExpectedArrived.arrived", numberofArrivedItem), UpdateFlags.Multi);
     
       }
       public void UpdateExpected(string supplierOrderId, int numberofExpected, string ean)
       {
           GroupedItemsCollection.Update(Query.And(Query.EQ("supplierOrderId", supplierOrderId), Query.EQ("ean", ean)), Update.Set("plus.OrderedExpectedArrived.expected", numberofExpected), UpdateFlags.Multi);
       }

       public void UpdateMissing(string supplierOrderId, int numberofMissingItems, string ean) {

           GroupedItemsCollection.Update(Query.And(Query.EQ("supplierOrderId", supplierOrderId), (Query.EQ("ean", ean))),
                                                    Update.Set("plus.OrderedExpectedArrived.missing", numberofMissingItems), UpdateFlags.Multi);
       }

       public void UpdateIsEdittedSpecificItem(string supplierOrderId, bool isEditted, string ean)
       {

           GroupedItemsCollection.Update(Query.And(Query.EQ("supplierOrderId", supplierOrderId), Query.EQ("ean", ean)), Update.Set("plus.OrderedExpectedArrived.expectedIsEdited", isEditted), UpdateFlags.Multi);
       }

       public void UpdateIsEditted(string supplierOrderId, bool isEditted)
       {
           GroupedItemsCollection.Update(Query.EQ("supplierOrderId", supplierOrderId), Update.Set("plus.OrderedExpectedArrived.expectedIsEditted", isEditted), UpdateFlags.Multi);

       }

       public int GetPackingUnitSize(string ean)
       {
           int unitsize = 0;

           try
           {
               unitsize = GroupedItemsCollection.FindOne(Query.EQ("ean", ean)).packingUnitsToOrderFromSupplier;
           }
           catch { }



           return unitsize;
       }

       
    }
}

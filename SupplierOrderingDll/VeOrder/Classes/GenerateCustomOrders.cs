﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierOrderingDLL.VeOrder.Classes
{
    public class GenerateCustomOrders 
    {

        public GenerateCustomOrders() { }

       
        public void Generate(string suppliername, List<GroupedItemsEntity> orderItems, string username, string vePrice)
        {
            new Others().UpdateStatus(suppliername);

            ////create the textfile
            //TextFileMaker createFile = new TextFileMaker();
            //filename = createFile.WriteFile(suppliername, orderItems,supplierOrderId);
            //process the orders by updating calculated collections and saving to groupeditems collections
            
            new ProcessOrderItems(orderItems, suppliername, username, vePrice);

            new Others().UpdateStatus(suppliername, true);
        }

       

        public string ExportCustomOrder(string suppliername, List<GroupedItemsEntity> orderItems, string supplierOrderId)
        {

            //create the textfile
            TextFileMaker createFile = new TextFileMaker();
            return createFile.WriteFile(suppliername, orderItems, supplierOrderId);

    
        }

        public string ExportOrderHistory( string supplierOrderId,List<GroupedItemsEntity> orderItems){

            TextFileMaker createFile = new TextFileMaker();
            return createFile.WriteFile_OrderHistory(supplierOrderId, orderItems);
        
        }
    }
}

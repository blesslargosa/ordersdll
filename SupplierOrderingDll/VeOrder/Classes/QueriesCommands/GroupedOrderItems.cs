﻿using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierOrderingDLL.VeOrder.Classes.QueriesCommands
{
    public class GroupedOrderItems : DBCollections
    {
        public void UpdateIsEdittedSpecificItem(string supplierOrderId, bool isEditted, string ean)
        {

            GroupedItemsCollection.Update(Query.And(Query.EQ("supplierOrderId", supplierOrderId), Query.EQ("ean", ean)), Update.Set("plus.OrderedExpectedArrived.expectedIsEditted", isEditted), UpdateFlags.Multi);
        }

        public void UpdateIsEditted(string supplierOrderId, bool isEditted)
        {
            GroupedItemsCollection.Update(Query.EQ("supplierOrderId", supplierOrderId), Update.Set("plus.OrderedExpectedArrived.expectedIsEditted", isEditted), UpdateFlags.Multi);

        }
        public int GetPackingUnitSize(string ean)
        {
            int unitsize = 0;

            try
            {
                unitsize = GroupedItemsCollection.FindOne(Query.EQ("ean", ean)).packingUnitsToOrderFromSupplier;
            }
            catch { }



            return unitsize;
        }

        public List<GroupedItemsEntity> GetAllOrdersByEan(string ean, string days)
        {

            return GroupedItemsCollection.Find(Query.And(Query.GTE("singleOrders.PurchasedDate", DateTime.Now.AddDays(Convert.ToInt32(days) * -1)), Query.EQ("ean", ean))).

                SetFields(Fields.Include("ean")
                                .Include("totalPackingUnitsPrice")
                                .Include("singleOrders.PurchasedDate")).ToList();


        }
    }
}
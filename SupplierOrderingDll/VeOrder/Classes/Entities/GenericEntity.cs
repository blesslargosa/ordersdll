﻿using DN_Classes.Entities;
using MongoDB.Bson.Serialization.Attributes;
using SupplierOrderingDLL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierOrderingDLL.VeOrder.Classes
{
    [BsonIgnoreExtraElements]
    public class GenericEntity
    {
        public string platform { get; set; }
        public string orderIdObjectId { get; set; }
        public string ean { get; set; }
        public string artnr { get; set; }
        public string supplier { get; set; }
        public int multiplier { get; set; }
        public int veSize { get; set; }
        public double price { get; set; }
        public string sku { get; set; }
        public DateTime minPurchasedDate { get; set; }
        public int warehouseStock { get; set; }
        public CalculatedProfitEntity singleorder { get; set; }

    }
}

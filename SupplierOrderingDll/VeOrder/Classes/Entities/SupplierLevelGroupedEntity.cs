﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierOrderingDLL.VeOrder.Classes.Entities
{
    public class SupplierLevelGroupedEntity
    {
        public string supplier { get; set; }
        public double VePrice { get; set; }
        public List<GroupedItemsEntity> items { get; set; }
        public DateTime minPurchasedDate { get; set; }
        public bool exported16hr { get; set; }
        public DateTime lastExportedDate { get; set; }
        public int dayspassedlastexported { get; set; }
        public string status { get; set; }
    }
}

﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierOrderingDLL.VeOrder.Classes.Entities
{
    [BsonIgnoreExtraElements]
    public class GroupedSupplierOrderIdItems
    {
        public string supplierOrderId { get; set; }
        public List<GroupedItemsEntity> groupedOrders { get; set; }
        public DateTime dateExported { get; set; }
        public bool orderedFromSupplier { get; set; }
        public string exportedBy { get; set; }
        public double orderTotalVePrice { get; set; }
        public BillDetails billdetails { get; set; }


    }

    public class BillDetails{

        public string billFileName { get; set; }
        public string billPrice { get; set; }
        public string billComment { get; set; }
    
    }
}

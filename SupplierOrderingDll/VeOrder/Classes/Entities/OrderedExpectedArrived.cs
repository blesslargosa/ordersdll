﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierOrderingDLL.VeOrder.Classes.Entities
{
    public class OrderedExpectedArrived
    {

            public string ean { get; set; }
            public int ordered { get; set; }
            public int expected { get; set; }
            public int arrived { get; set; }

    }
}

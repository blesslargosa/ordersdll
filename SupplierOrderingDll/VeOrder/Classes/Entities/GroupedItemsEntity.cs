﻿using DN_Classes.Entities;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using SupplierOrderingDLL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierOrderingDLL.VeOrder.Classes
{
    [BsonIgnoreExtraElements]
    public class GroupedItemsEntity
    {
        public string supplierOrderId { get; set; }

        public string ean { get; set; }

        public string supplier { get; set; }

        public string supplierSku { get; set; }

        public int packingUnitSize { get; set; }

        public double buyPrice { get; set; }

        public double totalPackingUnitsPrice { get; set; }

        public List<CalculatedProfitEntity> singleOrders { get; set; }

        public int packingUnitsToOrderFromSupplier { get; set; }

        public int itemsInStock { get; set; }

        public int numberOfOrderedItems { get; set; }

        public int itemsToOrderFomSupplier { get; set; }

        public DateTime minPurchasedDate { get; set; }

        public string sellerSKU { get; set; }

        public string SellerPrefix { get; set; }

        public string asin { get; set; }

        public int multiplier { get; set; }

        public BsonDocument plus { get; set; }


    }
}

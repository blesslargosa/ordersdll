﻿using DN_Classes.Entities;
using MongoDB.Bson;
using MongoDB.Driver.Builders;
using SupplierOrderingDLL.Entities;
using SupplierOrderingDLL.VeOrder.Interfaces;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SupplierOrderingDLL.VeOrder.Classes
{
    public class ReOrder : DBCollections, IPlatforms
    {
        public List<CalculatedProfitEntity> Orders { get; set; }
        public List<GenericEntity> GenericFields { get; set; }
  

        public ReOrder(bool fetch) {

            this.Orders = GetOrdersOriginalEntity();
           
            this.GenericFields = PrepareItems();
           

        }

        public ReOrder() { }

        public List<CalculatedProfitEntity> GetOrdersOriginalEntity()
        {
           var reorders = ReOrderCollection.Find(Query.Or
                (
                Query.And(
                Query.NotExists("plus.supplierOrderId"),
                Query.NotExists("plus.excludeFromOrdering"),
                Query.NE("SupplierName","not available"),
                Query.NE("AmazonOrder.FulfillmentChannel", "AFN"))
                ,
                Query.And(
                Query.NotExists("plus.supplierOrderId"),
                Query.Exists("plus.excludeFromOrdering"),
                Query.EQ("plus.excludeFromOrdering", false),
                Query.NE("SupplierName", "not available"),
                Query.NE("AmazonOrder.FulfillmentChannel", "AFN"))
                )).SetLimit(setlimit).Where(x => x.plus.Contains("note") && !x.plus["note"].AsBsonValue.ToString().Contains("test")).ToList();

         
            return reorders;
        }

       

        public List<GenericEntity> PrepareItems()
        {
            ConcurrentBag<GenericEntity> orderItems = new ConcurrentBag<GenericEntity>();

            ParallelOptions po = new ParallelOptions();
            po.MaxDegreeOfParallelism = 5;

            Parallel.ForEach(this.Orders, po, (order) =>
            {
                lock (new object())
                {
                    Others others = new Others();
                    GenericEntity orderItem = new GenericEntity()
                    {
                        platform = "reorder",
                        orderIdObjectId = GetPlatformOrderIdObjectId(order),
                        ean = order.Ean,
                        artnr = order.ArticleNumber,
                        supplier = order.SupplierName,
                        multiplier = others.GetMultiplier(order.SellerSku),
                        veSize = order.VE,
                        sku = order.SellerSku,
                        minPurchasedDate = order.PurchasedDate,
                        warehouseStock = others.GetStockFromWareHouse(order.Ean),
                        price = order.Item.NewBuyPrice,
                        singleorder = AppendPlatformOnCalculatedEntity(order)
                    };
                    orderItems.Add(orderItem);
                }
            });
            List<GenericEntity> gen = new List<GenericEntity>(orderItems);
            return gen;

        }

        private CalculatedProfitEntity AppendPlatformOnCalculatedEntity(CalculatedProfitEntity order)
        {
            BsonDocument doc = new BsonDocument();
            doc.Add(new BsonElement("platform", "reorder"));

            order.plus = doc;

            return order;
        }

        private string GetPlatformOrderIdObjectId(CalculatedProfitEntity order)
        {
            if (order.EbayOrder != null) { return order.EbayOrder.OrderID; } else {
                if (order.AmazonOrder != null) { return order.AmazonOrder.AmazonOrderId; } else { return order._id.ToString(); };
            }
        }

        public List<ObjectId> GetSpecificOrderIds(List<string> orderIds)
        {
            List<ObjectId> all = new List<ObjectId>();
            foreach (string orderId in orderIds)
            {
                Match match = Regex.Match(orderId, "^[0-9a-fA-F]{24}$");
                if (match.Success)
                {
                    all.Add(ObjectId.Parse(orderId));
                }
                else
                {
                    var amazonReOrder = ReOrderCollection.Find(Query.EQ("AmazonOrder.AmazonOrderId", orderId)).Select(x => x._id).ToList();
                    var ebayReOrder = ReOrderCollection.Find(Query.EQ("EbayOrder.EbayOrderId", orderId)).Select(x => x._id).ToList();
                    all.AddRange(amazonReOrder);
                    all.AddRange(ebayReOrder);
                }
               
            }
            return all;
        }


        public void UpdateCalculatedOrderPlus(ObjectId objectId, string supplierOrderId, DateTime exportedDate)
        {
            string obj = objectId.ToString().Replace("{", "").Replace("}", "");

            var order = ReOrderCollection.FindOne(Query.EQ("_id", ObjectId.Parse(obj)));

            if (order.plus == null || !order.plus.Contains("supplierOrderId"))
            {
                ReOrderCollection.Update(Query.EQ("_id", ObjectId.Parse(obj)),
                             Update.Set("plus.supplierOrderId", supplierOrderId)
                             .Set("plus.ordered", true).Set("plus.excludeFromOrdering", true).Set("plus.exportedDate", exportedDate));
            }
        }

        public List<CalculatedProfitEntity> GetOrdersByEan(string ean)
        {
            return ReOrderCollection.Find(Query.EQ("Ean", ean)).ToList();
        }


        public BsonDocument GetSpecificBsonOrder(ObjectId id)
        {
            return ReOrderCollectionBson.FindOne(Query.EQ("_id", id));
        }
    }
    


    
}

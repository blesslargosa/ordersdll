﻿using DN_Classes.Entities;
using MongoDB.Bson;
using MongoDB.Driver.Builders;
using ProcuctDB;
using SupplierOrderingDLL.Entities;
using SupplierOrderingDLL.ReOrdering.Interface;
using SupplierOrderingDLL.VeOrder.Interfaces;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SupplierOrderingDLL.VeOrder.Classes
{
    public class Amazon : DBCollections,IPlatforms,IReOrder
    {
        public List<CalculatedProfitEntity> Orders { get; set; }
        public List<GenericEntity> GenericFields { get; set; }
        
      

        public Amazon() { }

        public Amazon(bool fetch) {

            this.Orders = GetOrdersOriginalEntity();
          
            this.GenericFields = PrepareItems();

        }


        public List<CalculatedProfitEntity> GetOrdersOriginalEntity()
        {
            var amazonOrders = Collection.Find(
                       Query.Or(
                           Query.And(
                         
                           Query.NotExists("plus.supplierOrderId"),
                           Query.NotExists("plus.excludeFromOrdering"),
                           Query.NE("AmazonOrder.FulfillmentChannel", "AFN")
                           ),
                           Query.And(
                           Query.NotExists("plus.supplierOrderId"),
                           Query.Exists("plus.excludeFromOrdering"),
                           Query.EQ("plus.excludeFromOrdering", false),
                           Query.NE("AmazonOrder.FulfillmentChannel", "AFN")
                           )
                       )).SetLimit(setlimit).SetFields(Fields.Include("_id")
                       .Include("Agentname")
                       .Include("SellerSku")
                       .Include("SupplierName")
                       .Include("Ean")
                       .Include("ArticleNumber")
                       .Include("VE")
                       .Include("PurchasedDate")
                       .Include("Item.NewBuyPrice")
                       .Include("Item.Stock")
                       .Include("AmazonOrder.AmazonOrderId")
                       .Include("AmazonOrder.PurchaseDate")
                       .Include("plus"))
                       .ToList();
           
            return amazonOrders;
        }
      

        public List<GenericEntity> PrepareItems()
        {
            ConcurrentBag<GenericEntity> orderItems = new ConcurrentBag<GenericEntity>();
            ParallelOptions po = new ParallelOptions();
            po.MaxDegreeOfParallelism = 5;

            Parallel.ForEach(this.Orders, po, (order) => {

                lock (new object())
                {
                    Others others = new Others();
                    GenericEntity orderItem = new GenericEntity()
                    {

                        platform = "amazon",
                        orderIdObjectId = order.AmazonOrder.AmazonOrderId,
                        ean = order.Ean,
                        artnr = order.ArticleNumber,
                        supplier = order.SupplierName,
                        multiplier = others.GetMultiplier(order.SellerSku),
                        veSize = order.VE,
                        sku = order.SellerSku,
                        minPurchasedDate = order.PurchasedDate,
                        warehouseStock = others.GetStockFromWareHouse(order.Ean),
                        price = order.Item.NewBuyPrice,
                        singleorder = AppendPlatformOnCalculatedEntity(order)
                    };

                    orderItems.Add(orderItem);
                }
            
            });


            List<GenericEntity> gen = new List<GenericEntity>(orderItems);
            return gen;

        }

        private CalculatedProfitEntity AppendPlatformOnCalculatedEntity(CalculatedProfitEntity order)
        {
            BsonDocument doc = new BsonDocument();
            doc.Add(new BsonElement("platform", "amazon"));

            order.plus = doc;

            return order;
        }


        public List<ObjectId> GetSpecificOrderIds(List<string> orderIds)
        {
            List<ObjectId> all = new List<ObjectId>();
            foreach (string orderId in orderIds)
            {
                var ids = Collection.Find(Query.EQ("AmazonOrder.AmazonOrderId", orderId)).Select(x => x._id).ToList();
             all.AddRange(ids);

            }
            return all;
        }


        public void UpdateCalculatedOrderPlus(ObjectId objectId, string supplierOrderId, DateTime exportedDate)
        {
            string obj = objectId.ToString().Replace("{", "").Replace("}", "");

            var order = Collection.FindOne(Query.EQ("_id", ObjectId.Parse(obj)));

            if (order.plus == null || !order.plus.Contains("supplierOrderId"))
            {
                Collection.Update(Query.EQ("_id", objectId),
                             Update.Set("plus.supplierOrderId", supplierOrderId)
                             .Set("plus.ordered", true).Set("plus.excludeFromOrdering", true).Set("plus.exportedDate", exportedDate));
            }
            
           
            
        }


        public List<CalculatedProfitEntity> GetOrdersByEan(string ean)
        {
            return Collection.Find(Query.EQ("Ean", ean)).ToList();
        }

        public List<string> GetMultipleEansByOrderId(string orderId) {

            var orders = Collection.Find(Query.EQ("AmazonOrder.AmazonOrderId", orderId)).ToList();

            return orders.Select(x => x.Ean).ToList();
        
        }


        public IReOrder GetOrderItemPlatform(string ean, string orderId)
        {
            var item = Collection.FindOne(Query.And(Query.EQ("Ean", ean), Query.EQ("AmazonOrder.AmazonOrderId", orderId)));

            if (item != null)
            {
                return new Amazon();
            }
            return null;
        }


        public CalculatedProfitEntity GetSpecificOrderByOrderId(string ean, string orderId)
        {
            return Collection.FindOne(Query.And(Query.EQ("Ean", ean), Query.EQ("AmazonOrder.AmazonOrderId", orderId)));
        }

        public void UpdateReOrderedItem(string ean, string orderId, DateTime ReOrderDate)
        {
            Collection.Update(Query.And(Query.EQ("Ean", ean), Query.EQ("AmazonOrder.AmazonOrderId", orderId)),
            Update.
            Push("plus.reOrderDate", ReOrderDate));
        }






        public BsonDocument GetSpecificBsonOrder(ObjectId id)
        {
            return AmazonCollectionBson.FindOne(Query.EQ("_id", id));
        }


        public string GetAgentName(string orderId)
        {
            var order =  Collection.Find(Query.EQ("AmazonOrder.AmazonOrderId", orderId)).SetFields(Fields.Include("Agentname")).ToList();

            return order.First().Agentname;
        }
    }
}

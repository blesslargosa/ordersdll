﻿using DN_Classes.Entities;
using MongoDB.Bson;
using MongoDB.Driver.Builders;
using SupplierOrderingDLL.Entities;
using SupplierOrderingDLL.ReOrdering.Interface;
using SupplierOrderingDLL.VeOrder.Interfaces;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SupplierOrderingDLL.VeOrder.Classes
{
    public class Ebay : DBCollections, IPlatforms, IReOrder
    {
        public List<CalculatedProfitEntity> Orders { get; set; }
        public List<GenericEntity> GenericFields { get; set; }
     

        public Ebay(bool fetch) {

            this.Orders = GetOrdersOriginalEntity();
         
            this.GenericFields = PrepareItems();
            
        }

        public Ebay() { }

        public List<CalculatedProfitEntity> GetOrdersOriginalEntity()
        {
            var ebayOrders = EbayCalculatedCollection.Find(Query.Or
                (
                Query.And(
                Query.NotExists("plus.supplierOrderId"),
                Query.NotExists("plus.excludeFromOrdering"))
                ,
                Query.And(
                Query.NotExists("plus.supplierOrderId"),
                Query.Exists("plus.excludeFromOrdering"),
                Query.EQ("plus.excludeFromOrdering", false))
                )).SetLimit(setlimit).SetFields(Fields.Include("_id")
                       .Include("Agentname")
                       .Include("SellerSku")
                       .Include("SupplierName")
                       .Include("Ean")
                       .Include("ArticleNumber")
                       .Include("VE")
                       .Include("PurchasedDate")
                       .Include("Item.NewBuyPrice")
                       .Include("Item.Stock")
                       .Include("EbayOrder.OrderID")
                
                       .Include("plus"))
                       .ToList();


           
            return ebayOrders;
        }

      
        public List<GenericEntity> PrepareItems()
        {
            ConcurrentBag<GenericEntity> orderItems = new ConcurrentBag<GenericEntity>();

             ParallelOptions po = new ParallelOptions();
            po.MaxDegreeOfParallelism = 5;

            Parallel.ForEach(this.Orders, po, (order) =>
            {
                lock(new object()){
                Others others = new Others();
                GenericEntity orderItem = new GenericEntity()
                {
                    platform = "ebay",
                    orderIdObjectId = order.EbayOrder.OrderID,
                    ean = order.Ean,
                    artnr = order.ArticleNumber,
                    supplier = order.SupplierName,
                    multiplier = others.GetMultiplier(order.SellerSku),
                    veSize = order.VE,
                    sku = order.SellerSku,
                    minPurchasedDate = order.PurchasedDate,
                    warehouseStock = others.GetStockFromWareHouse(order.Ean),
                    price = order.Item.NewBuyPrice,
                    singleorder = AppendPlatformOnCalculatedEntity(order)
                };

                orderItems.Add(orderItem);
                }
            });

            List<GenericEntity> gen = new List<GenericEntity>(orderItems);
            return gen;

        }

        private CalculatedProfitEntity AppendPlatformOnCalculatedEntity(CalculatedProfitEntity order)
        {
            BsonDocument doc = new BsonDocument();
            doc.Add(new BsonElement("platform", "ebay"));

            order.plus = doc;

       
            return order;
        }

        public List<ObjectId> GetSpecificOrderIds(List<string> orderIds)
        {
            List<ObjectId> all = new List<ObjectId>();
            foreach (string orderId in orderIds)
            {
                var ids = EbayCalculatedCollection.Find(Query.EQ("EbayOrder.OrderID", orderId)).Select(x => x._id).ToList();

                all.AddRange(ids);

            }
            return all;
        }


        public void UpdateCalculatedOrderPlus(ObjectId objectId, string supplierOrderId, DateTime exportedDate)
        {
            string obj = objectId.ToString().Replace("{", "").Replace("}", "");

            var order = EbayCalculatedCollection.FindOne(Query.EQ("_id", ObjectId.Parse(obj)));

            if (order.plus == null || !order.plus.Contains("supplierOrderId"))
            {
                EbayCalculatedCollection.Update(Query.EQ("_id", ObjectId.Parse(obj)),
                             Update.Set("plus.supplierOrderId", supplierOrderId)
                             .Set("plus.ordered", true).Set("plus.excludeFromOrdering", true).Set("plus.exportedDate", exportedDate));
            }
        }


        public List<CalculatedProfitEntity> GetOrdersByEan(string ean)
        {
            return EbayCalculatedCollection.Find(Query.EQ("Ean", ean)).ToList();
        }

        public List<string> GetMultipleEansByOrderId(string orderId)
        {

            var orders = EbayCalculatedCollection.Find(Query.EQ("EbayOrder.OrderID", orderId)).ToList();

            return orders.Select(x => x.Ean).ToList();

        }

        public IReOrder GetOrderItemPlatform(string ean, string orderId)
        {
            var item = EbayCalculatedCollection.FindOne(Query.And(Query.EQ("Ean", ean), Query.EQ("EbayOrder.OrderID", orderId)));

            if (item != null)
            {
                return new Ebay();
            }
            return null;
        }

        public CalculatedProfitEntity GetSpecificOrderByOrderId(string ean, string orderId)
        {
            return Collection.FindOne(Query.And(Query.EQ("Ean", ean), Query.EQ("EbayOrder.EbayOrderID", orderId)));
        }

        public void UpdateReOrderedItem(string ean, string orderId, DateTime ReOrderDate)
        {
            EbayCalculatedCollection.Update(Query.And(Query.EQ("Ean", ean), Query.EQ("EbayOrder.OrderID", orderId)),
            Update.
            Push("plus.reOrderDate", ReOrderDate));
        }


        public BsonDocument GetSpecificBsonOrder(ObjectId id)
        {
            return EbayCollectionBson.FindOne(Query.EQ("_id", id));
        }


        public string GetAgentName(string orderId)
        {
            var order = EbayCalculatedCollection.Find(Query.EQ("EbayOrder.OrderID", orderId)).SetFields(Fields.Include("Agentname")).ToList();

            return order.First().Agentname;
        }
    }
}

﻿using DN_Classes.Entities;
using MongoDB.Bson;
using SupplierOrderingDLL.Entities;
using SupplierOrderingDLL.VeOrder.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierOrderingDLL.VeOrder.Interfaces
{
   public interface IPlatforms
    {
       List<CalculatedProfitEntity> GetOrdersOriginalEntity();
      
       List<GenericEntity> PrepareItems();
       List<ObjectId> GetSpecificOrderIds(List<string> orderIds);
       void UpdateCalculatedOrderPlus(ObjectId objectId, string supplierOrderId,DateTime exportedDate);
       BsonDocument GetSpecificBsonOrder(ObjectId id);
       
    }
}

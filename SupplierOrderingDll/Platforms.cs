﻿using SupplierOrderingDLL.ReOrdering.Interface;
using SupplierOrderingDLL.VeOrder.Classes;
using SupplierOrderingDLL.VeOrder.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierOrderingDLL
{
    public class Platforms
    {
        public List<IPlatforms> GetAllPlatforms() {

            List<IPlatforms> platforms = new List<IPlatforms>();
            platforms.Add(new Amazon());
            platforms.Add(new Ebay());
            platforms.Add(new ReOrder());


            return platforms;
        }

        public List<IReOrder> GetAllReOrderPlatforms() {
            List<IReOrder> reorderPlatforms = new List<IReOrder>();
            reorderPlatforms.Add(new Amazon());
            reorderPlatforms.Add(new Ebay());

            return reorderPlatforms;
        }

    }
}

﻿using DN_Classes.Supplier;
using MongoDB.Bson;
using SupplierOrderingDLL.OrderPurchaseFormatting.ExportingFormats;
using SupplierOrderingDLL.OrderPurchaseFormatting.FormatIndicators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierOrderingDLL.OrderPurchaseFormatting.FormattingFactory
{
    public class ExportFormatFactoryBasic : IFormatFactory
    {
        private IFormatIndicator FormatIndicator;

        public ExportFormatFactoryBasic(IFormatIndicator formatIndicator)
        {
            this.FormatIndicator = formatIndicator;
        }

        public IExportFormat GetExportFormat()
        {
            IExportFormat exportFormat = null;

            string indicator = FormatIndicator.GetIndicator();

            ExportFormatList formatList = new ExportFormatList();

            exportFormat = formatList.GetExportFormat(indicator);

            return exportFormat;
        }

    }
}

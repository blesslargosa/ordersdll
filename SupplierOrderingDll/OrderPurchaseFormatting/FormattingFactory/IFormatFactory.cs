﻿
using SupplierOrderingDLL.OrderPurchaseFormatting.ExportingFormats;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierOrderingDLL.OrderPurchaseFormatting.FormattingFactory
{
    public interface IFormatFactory
    {
        /// <summary>
        /// returns the export format
        /// </summary>
        /// <returns>IExportFormat</returns>
        IExportFormat GetExportFormat();
    }
}

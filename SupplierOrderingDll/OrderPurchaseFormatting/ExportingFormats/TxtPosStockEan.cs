﻿using SupplierOrderingDLL.VeOrder.Classes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierOrderingDLL.OrderPurchaseFormatting.ExportingFormats
{
    public class TxtPosStockEan : IExportFormat
    {
        private List<GroupedItemsEntity> Groupedorders;
        private string Filename;

        public void Prepare(List<GroupedItemsEntity> groupedorders, string filename)
        {
            this.Groupedorders = groupedorders.Where(x => x.packingUnitsToOrderFromSupplier > 0).OrderByDescending(x => x.packingUnitsToOrderFromSupplier).ToList();
            this.Filename = filename;
        }

        public void Write()
        {
            Groupedorders = Groupedorders.Where(x => !String.IsNullOrEmpty(x.ean)).ToList();
            List<string> posStockEan = new List<string>();
            int count = 1;
            foreach (var x in Groupedorders) {
                posStockEan.Add("Pos." + count + "\t" + x.packingUnitsToOrderFromSupplier + " Stück" + "\t" + x.supplierSku);
            count++;
            }
            File.WriteAllText(Filename, String.Join("\r\n", posStockEan));
        }
    }
}

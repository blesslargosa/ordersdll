﻿using SupplierOrderingDLL.VeOrder.Classes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierOrderingDLL.OrderPurchaseFormatting.ExportingFormats
{
    public class TxtBasic : IExportFormat
    {
        private List<GroupedItemsEntity> Groupedorders;
        private string Filename;

        public void Prepare(List<GroupedItemsEntity> groupedorders, string filename)
        {
            this.Groupedorders = groupedorders.Where( x => x.packingUnitsToOrderFromSupplier > 0).OrderByDescending(x => x.packingUnitsToOrderFromSupplier).ToList() ;
            this.Filename = filename;
        }

        public void Write()
        {
            List<string> details = Groupedorders.AsParallel().WithDegreeOfParallelism(5)
                .Select(x => x.packingUnitsToOrderFromSupplier + "\t" + 
                    GetVEorSTK(x.packingUnitSize) + "\t" +
                    x.supplierSku + "\t" + x.ean).ToList();

            File.WriteAllText(Filename, String.Join("\r\n", details));
        }

        private object CheckNullValue(List<DN_Classes.Entities.CalculatedProfitEntity> list)
        {
            throw new NotImplementedException();
        }
       
        //public void Write()
        //{
        //    List<string[]> lineList = new List<string[]>();

        //    string separator = "\t";

        //    lineList.Add(GetHeader());

        //    foreach (GroupedItemsEntity groupedItemsEntity in Groupedorders)
        //    {
        //        int pieces = groupedItemsEntity.packingUnitSize;

        //        string stk = GetVEorSTK(pieces);

        //        string[] line = new string[4]
        //        {
        //                 groupedItemsEntity.packingUnitsToOrderFromSupplier.ToString(),
        //                 stk,
        //                 groupedItemsEntity.singleOrders.GroupBy(x =>x.ArticleNumber).First().Key,
        //                 groupedItemsEntity.ean
                         
                         
        //        };

        //        lineList.Add(line);
        //    }

        //    WriteToFile(lineList, separator);
        //}
        //private string[] GetHeader()
        //{
        //    string[] header = new string[4]
        //    {
        //        "QTY",
        //        "STK/VE",
        //        "ARTNR",
        //        "EAN"
        //    };

        //    return header;
        //}
        //private void WriteToFile(List<string[]> lineList, string separator)
        //{
        //    StringBuilder stringBuilder = new StringBuilder();
        //    foreach (string[] line in lineList)
        //        stringBuilder.AppendLine(string.Join(separator, line));

        //    File.AppendAllText(Filename, stringBuilder.ToString());
        //}

        private string GetVEorSTK(int pieces)
        {
            string stk = "VE";
            if (pieces == 1)
            {
                stk = "STK";
            }

            return stk;
        }

        
    }
}

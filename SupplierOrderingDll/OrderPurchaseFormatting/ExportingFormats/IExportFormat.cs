﻿using SupplierOrderingDLL.VeOrder.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierOrderingDLL.OrderPurchaseFormatting.ExportingFormats
{
    public interface IExportFormat
    {
        /// <summary>
        /// prepares the data needed
        /// </summary>
        /// <param name="groupedorders"></param>
        /// <param name="filename"></param>
        void Prepare(List<GroupedItemsEntity> groupedorders, string filename);

        /// <summary>
        /// writes the file for order exporting
        /// </summary>
        void Write();
    }
}

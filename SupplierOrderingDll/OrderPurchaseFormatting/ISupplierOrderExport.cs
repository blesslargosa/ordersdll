﻿
using SupplierOrderingDLL.OrderPurchaseFormatting.ExportingFormats;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierOrderingDLL.OrderPurchaseFormatting
{
    public interface ISupplierOrderExport
    {
        /// <summary>
        /// calls the write method of IExportFormat argument
        /// </summary>
        /// <param name="exportFormat"></param>
        void Write(IExportFormat exportFormat);
    }
}
